# File:       annBF/simple_bmode.py
# Author:     Dongwoon Hyun (dongwoon.hyun@stanford.edu)
# Created on: 2019-07-18

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import tensorflow as tf
from tensorflow.keras import layers, Model
from freeze_models import save_model_tf_keras as save_model

UFF_FILENAME = "bmode_network.uff"
NCHANS = 128


def main():
    # Define an input size
    ncols = 16  # This value will be replaced by actual image size
    nrows = 16  # This value will be replaced by actual image size
    sz = [NCHANS, ncols, nrows * 2]  # 2 for interleaved real/imag

    # Create model containing desired operations
    inputs = layers.Input(shape=sz, dtype=tf.float32, name="input")
    outputs = layers.Lambda(tf_make_bmode_image, name="output")(inputs)

    model = Model(inputs=inputs, outputs=outputs)

    # Save model to frozen graph .pb file
    save_model(model, UFF_FILENAME)


def tf_make_bmode_image(x):
    # Sum channel dimension
    # NOTE: reduce_sum is not supported by TensorRT. Instead, hack it using a
    #       1x1 conv2d of ones with 1 output channel.
    f = tf.constant(1, shape=[1, 1, NCHANS, 1], dtype=tf.float32)
    x = tf.nn.conv2d(x, f, strides=[1, 1, 1, 1], padding="SAME", data_format="NCHW")
    # x = tf.reduce_sum(x, axis=1, keepdims=True)  # Not compatible with TensorRT yet

    # Compute the envelope
    # NOTE: The real/imag components are stored in axis=4, which doesn't play
    #       nicely with TensorRT. Instead, square all values and then use a
    #       two-dimensional convolution with stride 2 in the row dimension.
    # x = tf.norm(x, axis=4, keepdims=False)  # Not compatible with TensorRT yet
    x = tf.multiply(x, x)
    f = tf.constant(1, shape=[1, 2, 1, 1], dtype=tf.float32)
    x = tf.nn.conv2d(
        x, f, strides=[1, 1, 1, 2], padding="SAME", data_format="NCHW", name="output"
    )

    return x


if __name__ == "__main__":
    main()
