classdef UIArray < ObjectArray
	properties
		Labels = {}
	end
	methods
		function addSlider(self, loc, label, minval, maxval, curval, ...
				steps, strformat, callback)
			import vsv.seq.uicontrol.VsSliderControl;
			self.push(...
				struct( ...
					'Control', VsSliderControl( ...		
						'LocationCode', loc, ...
						'Label', label, ...
						'SliderMinMaxVal', [minval, maxval, curval], ...
						'SliderStep', [steps(1), steps(2)] / (maxval - minval), ...
						'ValueFormat', strformat, ...
						'Callback', callback)), ...
				label);				
		end
		function addButton(self, loc, label, callback)
			import vsv.seq.uicontrol.VsButtonControl;
			self.push( ...
				struct( ...
					'Control', VsButtonControl( ...		
						'LocationCode', loc, ...
						'Label', label, ...
						'Callback', callback)), ...
				label);
		end
		function addToggle(self, loc, label, callback)
			import vsv.seq.uicontrol.VsToggleButtonControl;
			self.push( ...
				struct( ...
					'Control', VsToggleButtonControl( ...		
						'LocationCode', loc, ...
						'Label', label, ...
						'Callback', callback)), ...
				label);
		end
		function addBGroup(self, loc, label, cases, callback)
			import vsv.seq.uicontrol.VsButtonGroupControl;
			self.push( ...
				struct( ...
					'Control', VsButtonGroupControl( ...		
						'LocationCode', loc, ...
						'Title', label, ...
						'PossibleCases', cases, ...
						'Callback', callback)), ...
				label);
		end
		function addStatement(self, statement, label)
			import vsv.seq.uicontrol.VsStatementOnly;
			self.push( ...
				struct( ...
					'Control', VsStatementOnly('Statement', statement)), ...
				label);
		end
	end
end