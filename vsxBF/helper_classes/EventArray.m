classdef EventArray < ObjectArray
	methods
		% Helper function to simplify adding a new Event to the current array.
		function addEvent(self, tx, rcv, recon, process, seqControl, infostr)
			self.push(...
				struct( ...
					'tx', tx, ...
					'rcv', rcv, ...,
					'recon', recon, ...
					'process', process, ...
					'seqControl', seqControl, ...
					'info', infostr), ...
				infostr);
		end

		% Create a whole frame's worth of Tx/Rx Events, incrementing only the tx and rcv.
		function addTxRxFrame(self, frameSize, tx_offset, rx_offset, ...
			recon, process, seqcontrol, infostr)
			for n = 1:frameSize
				self.addEvent(tx_offset+n, rx_offset+n, recon, process, ...
					seqcontrol, infostr);
			end
		end

		% We often need to modify the SeqControl of the most recently added event.
		function modifyPrevSeqControl(self, newSeqControl)
			self.data(end).seqControl = newSeqControl;
		end
	end
end