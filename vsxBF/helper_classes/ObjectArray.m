% Simple naive array wrapper class.
%   Meant to simplify appending new data without worrying about indexes.
classdef ObjectArray < handle
	properties(Access = public)
		data = []
		labels = {}
	end
	methods
		% Append a new object to the list, along with a label
		function push(self, newdata, newlabel)
			self.data = cat(2, self.data, newdata);
			self.labels = cat(2, self.labels, newlabel);
		end

		% Return the index of the previous object in the list
		function n = prevIdx(self)
            n = numel(self.data) - 1;
        end

		% Return the index of the most recent object in the list
		function n = currIdx(self)
            n = numel(self.data);
        end

		% Return the index of the next object to be added to the list
        function n = nextIdx(self)
            n = numel(self.data) + 1;
        end

		% Find the index of the object with label <str>
		function idx = get(self, str)
			idx = find(strcmp(self.labels, str));
		end
	end
end