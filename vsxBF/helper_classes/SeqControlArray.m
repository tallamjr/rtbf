classdef SeqControlArray < ObjectArray
	properties
		Labels = {}
	end
	methods
		% Custom method to easily add a SeqControl object by its
		% command, argument, condition, and user-supplied label
		function addSC(self, cmd, arg, con, label)
			self.push(...
				struct( ...
					'command', cmd, ...
					'argument', arg, ...
					'condition', con), ...
				label);
		end
	end
end