% This function is a wrapper for the real-time beamformer that can be used with both
% live imaging and offline processing.
% The top-level function can be invoked in two ways:
%   1. Initialization
%      The required inputs are structs P, R, Resource, Receive, TX, Trans, and TW.
%      The user can optionally supply pixPos (e.g. to match PData). Otherwise, pixPos
%      is automatically generated based on the parameters in R (e.g. fovWidth, rxDepth).
%   2. Execution
%      Once initialized, the user can pass the raw receive buffer as input. The code
%      automatically detects the number of ouputs and assigns them appropriately.

function varargout = process_L12_3v_bmode_synap(varargin)
    persistent isinit

    % Choose processing based on number of inputs
    if nargin == 7 || nargin == 8
        [varargout{1}, varargout{2}] = initialize(varargin{:});
        isinit = true;
    elseif nargin == 1 && ~isempty(isinit) && isinit
        nargs = nargout;
        varargout = execute(varargin{1}, nargs);
    elseif nargin == 1 && (isempty(isinit) || ~isinit)
        error('Cannot execute; processing code is not initialized yet.')
    else
        error(['Unexpected number of input arguments. Inputs should be ' ...
            'P, R, Resource, Receive, TX, Trans, TW, and pixPos (optional).']);
    end
end
    
function outputs = execute(rcvbuf, nargs)
    % Execute GPU beamformer
    outputs = cell(1, nargs);
    [outputs{1:nargs}] = L12_3v_bmode_synap(rcvbuf);
end
    
function [lat, ax] = initialize(P, R, Resource, Receive, TX, Trans, TW, pixPos)
    if ~strcmp(Trans.units, 'wavelengths')
        error('TODO: Provide support for Trans.units == ''mm''...');
    end

    % Parse structures
    nsamps	= Receive(1).endSample;
    spcy    = round(Receive(1).samplesPerWave);
    nxmits	= P.nxmits;
    elemPos = single(Trans.ElementPos(:,1:3));
    beamOri	= [];
    nchans = Resource.Parameters.numRcvChannels;
    if ~isfield(Receive, 'aperture')
        for i = 1:length(Receive)
            Receive(i).aperture = 1;
        end
    end
    beamDir	= zeros(nxmits, 2, 'single');
    time0wl = zeros(nxmits, 1, 'single');
    apers_0 = zeros(nxmits, 1, 'int32');
    cmap = zeros(nchans, nxmits, 'int32');
    for i = 1:nxmits
        beamDir(i,:) = TX(i).Steer;
        apers_0(i) = Receive(i).aperture - 1; % 1-indexed => 0-indexed
        cmap(:,i) = int32(1:128)' - 1;
        cmap(1:apers_0(i),i) = cmap(1:apers_0(i),i) + nchans;
        chanIdx = int32(1:128)' + apers_0(i);
        time0wl(i,:) = TW(1).peak + 2*Trans.lensCorrection + ...
            interp1(elemPos(chanIdx,1), TX(i).Delay, ...
            mean(elemPos(chanIdx,1)), 'spline', 'extrap');
    end
    % If only 128 unique channels are ever used, don't initialize unnecessary channels
    if numel(unique(apers_0)) == 1
        nelems = nchans;
        for i = 1:nxmits
            chanIdx = int32(1:128)' + apers_0(i);
            cmap(:,i) = mod(chanIdx-1, 128);
        end
    else
        nelems = size(elemPos,1);
    end
    % The samples per wavelength of the TX depends on whether useAsIQ mode is on.
    spwl = spcy;
    sampleMode = P.sampleMode;
    if R.useAsIQ == 0 && strcmp(P.sampleMode, 'NS200BW')
        sampleMode = 'HILBERT';
        spwl = 2*spcy;
    end
    % If no pixel positions are provided, automatically select according to R options
    if nargin == 7
        % Define pixel positions. For now, just make them come straight down from xdcr
        rcnOri = single(0:1/R.outspwl:R.fovWidth)';
        rcnOri = rcnOri - mean(rcnOri) + mean(elemPos(chanIdx,1));
        rcnOri = cat(2,rcnOri,0*rcnOri,0*rcnOri + P.rxStart);
        rcnDir = zeros(size(rcnOri,1),2,'single');
        % The samples per wavelength of the TX depends on whether NxFc mode is on.
        spwl = spcy;
        sampleMode = P.sampleMode;
        if R.useAsIQ == 0 && strcmp(P.sampleMode, 'NS200BW')
            sampleMode = 'HILBERT';
            spwl = 2*spcy;
        end
        nrows   = round((P.rxDepth-P.rxStart) * R.outspwl);
        % Compute delay profiles
        pixPos = computePixelPositions(rcnOri, rcnDir, nrows, R.outspwl);   
        pixPos = pixPos(pixPos(:,1,3)>=P.rxStart & pixPos(:,1,3) <= P.rxDepth,:,:);
    end
    [delRx,delTx] = makeDelayTables(elemPos, pixPos, beamOri, beamDir, spwl, time0wl-2*P.rxStart, apers_0);
    % apoTx = ones(size(pixPos,1), nxmits, size(pixPos,2), 'single');
    apoTx = makeTxApodTable(elemPos, pixPos, beamOri, beamDir, apers_0);
    apoRx = makeRxApodTable(elemPos, pixPos, R.fnum, apers_0);

    delTx = permute(delTx, [1 3 2]);
    delRx = permute(delRx, [1 3 2]);
    apoTx = permute(apoTx, [1 3 2]);
    apoRx = permute(apoRx, [1 3 2]);
    % Compute the axis labels
    ax  = double(pixPos(:,1,3) / (Trans.frequency*1e6) * Resource.Parameters.speedOfSound * 1000);
    lat = double(pixPos(1,:,1) / (Trans.frequency*1e6) * Resource.Parameters.speedOfSound * 1000);

    % Normalize
    apoRx = apoRx ./ mean(apoRx,3);

    % Initialize
    S = [];
    S.nsamps = nsamps;
    S.npulses = P.np;
    S.nxmits = nxmits;
    S.nelems = nelems;
    S.nchans = nchans;
    S.nrows = size(pixPos, 1);
    S.ncols = size(pixPos, 2);
    S.outspwl = R.outspwl;
    S.delTx = delTx;
    S.apoTx = apoTx;
    S.delRx = delRx;
    S.apoRx = apoRx;
    S.chanmap = cmap;
    S.sampleMode = sampleMode;
    S.interpMode = 'cubic'; % 'linear';
    S.synthesisMode = 0;  % 0: synthesize transmit; 1: synthesize receive
        
    % Initialize
    clear L12_3v_bmode_synap
    L12_3v_bmode_synap(S);
    
end