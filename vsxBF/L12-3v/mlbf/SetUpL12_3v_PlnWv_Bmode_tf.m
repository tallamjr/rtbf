% SetUpL12_3v_PlnWv_Bmode_tf.m:
% B-mode imaging using rtbf to perform plane wave compounding.
%
% This script utilizes the following gpuBF and vsxBF classes:
%   VSXDataFormatter
%   HilbertTransform (optional, if R.useAsIQ == 0)
%   FocusSynAp
%   ChannelSum
%   Bmode

clear all

%% Set paths and filenames
% WARNING: 	DATE AND TIME OF STUDY ARE CONSIDERED PROTECTED HEALTH INFORMATION. If you
% 			are collecting anonymized clinical data and wish to remain HIPAA compliant,
%			use namingScheme = 'anonymous';
%           Be sure to update the file metadata as well (date created, modified, etc.).
saveDir = './data';  % Directory where raw data is to be saved
namingScheme = 'anonymous';  % 'datestr' or 'anonymous'

% Find rtbf directory programmatically. (Located '../../../' of current script.)
tmp = split(which(mfilename), filesep);
rtbfdir = join(tmp(1:end-4), '/'); % ignore filename, go up 3 dirs
rtbfdir = rtbfdir{1};
% Add path to rtbf/vsxBF/functions
addpath(sprintf('%s/vsxBF/functions', rtbfdir));
addpath(sprintf('%s/vsxBF/helper_classes', rtbfdir));

%% Define acquisition parameters
% Hard parameters (cannot modify during acquisition)
P.na = 25;  % Number of plane wave angles
P.totalAngle = 10;  % Total angular span
P.configName = 'L12-3v_PlnWv_Bmode_tf';
P.sampleMode = 'NS200BW';  % [NS200BW, BS100BW, BS50BW] supported
% Modifiable parameters (using the GUI)
P.imageBufferSize = 10;  % Number of frames
P.rxStartMm = 0;  % Receive start depth
P.rxDepthMm = 30;  % Receive end depth
P.rxMaxDepthMm = 50;  % Maximum end depth
P.acqFPS = 100;	 % frames per second of data being saved
P.acqPRF = 10e3;  % 10kHz PRF
P.TxVoltage = 0;  % To be populated later

%% Define reconstruction parameters
% R will contain all of the parameters used at the time of acquisition.
R.fovWidthMm = 20;  % Width of the field of view
R.outspwl = 3;  % Output pixel grid density (samples per wavelength)
R.fnum = 2;  % F-number for transmit and receive apodization
R.brange = [-50 0];  % Displayed dynamic range w.r.t. maximum value
R.useAsIQ = 0;  % 0: RF data (NS200BW only), 1: direct IQ sampling
R.useMedianFilter = 0;  % Flag to post-process image with 3x3 median filter
R.uffFilename = sprintf('%s/annBF/bmode_network.uff', rtbfdir);

%% Populate the rest of P and R (do not modify!!!)
R.initialized = false;
% Use the right half of the aperture (aperture #65) for TX/RX
apers = 65 * ones(P.na,1);
% Set angles
if P.na == 1
	P.angles = 0;	P.dtheta = 0;
else
	P.angles = linspace(-P.totalAngle/2,P.totalAngle/2,P.na) * pi/180;
	P.dtheta = abs(diff(P.angles(1:2)));
end
P.np = 1; % No pulse compounding (e.g., pulse-inversion harmonic imaging)
if strcmp(P.sampleMode, 'NS200BW')
	P.spw = 4;
elseif strcmp(P.sampleMode, 'BS100BW')
	P.spw = 2;
elseif strcmp(P.sampleMode, 'BS50BW')
	P.spw = 1;
end
if R.useAsIQ == 0 && ~strcmp(P.sampleMode, 'NS200BW')
	error('To treat data as RF, P.sampleMode must be ''NS200BW''.')
end

% Flags for the external processing function
updatedRange = false;
quickUpdate = false; % Flag for small updates to mex function

%% Define system parameters.
Resource.Parameters.numTransmit = 128;  % number of transmit channels.
Resource.Parameters.numRcvChannels = 128;  % number of receive channels.
Resource.Parameters.simulateMode = 0;
Resource.Parameters.speedOfSound = 1540;

%% Specify Trans structure array.
Trans.name = 'L12-3v';
Trans.units = 'wavelengths';  % required in Gen3 to prevent default to mm units
Trans.frequency = 7.813;  % center frequency for harmonic imaging receive
Trans = computeTrans(Trans);  % L12-3v transducer is 'known' transducer so we can use computeTrans.
Trans.maxHighVoltage = 30;

%% Define lengths in wavelengths now that we have Trans.frequency
mmToWvln = @(mm) mm*1e-3 * Trans.frequency*1e6 / 1540;
wvlnToMm = @(wvln) wvln*1540 / (Trans.frequency*1e6) * 1e3;
P.rxStart = mmToWvln(P.rxStartMm);
P.rxDepth = mmToWvln(P.rxDepthMm);
R.fovWidth = mmToWvln(R.fovWidthMm);
P.rxMaxDepth = mmToWvln(P.rxMaxDepthMm);

%% Specify Media object. 'pt1.m' script defines array of point targets.
pt1;
Media.function = 'movePoints';

%% Specify Resources.
% Compute the rows per frame (must be a multiple of 128)
rpf = (P.rxMaxDepth+20) * 2 * P.spw * P.na;
rpf = ceil(rpf / 128) * 128;
% Live imaging buffer
Resource.RcvBuffer(1).datatype = 'int16';
Resource.RcvBuffer(1).rowsPerFrame = rpf;
Resource.RcvBuffer(1).colsPerFrame = Resource.Parameters.numRcvChannels;
Resource.RcvBuffer(1).numFrames = 10;
% Save data buffer
Resource.RcvBuffer(2).datatype = 'int16';
Resource.RcvBuffer(2).rowsPerFrame = rpf;
Resource.RcvBuffer(2).colsPerFrame = Resource.Parameters.numRcvChannels;
Resource.RcvBuffer(2).numFrames = P.imageBufferSize;

%% Specify Transmit waveform structure.
TW(1).type = 'parametric';
TW(1).Parameters = [Trans.frequency, 1, 2, 1];   % A, B, C, D

%% Specify TX structure array.
TX = repmat(struct('waveform', 1, ...
				   'Origin', [0, 0, 0], ...
				   'aperture', 1, ...
				   'Apod', ones(1,Resource.Parameters.numTransmit), ...
				   'focus', 0, ...
				   'Steer', [0, 0], ...
				   'Delay', zeros(1,Resource.Parameters.numTransmit)), 1, P.na);
% - Set event specific TX attributes.
for n = 1:P.na
	TX(n).aperture = apers(n);
	TX(n).Steer(1) = P.angles(n);
	TX(n).Delay = computeTXDelays(TX(n));
end

%% Specify TPC structures.
TPC(1).name = 'Imaging';
TPC(1).maxHighVoltage = 30;

%% Specify TGC Waveform structure.
TGC.CntrlPts = [442,599,728,795,914,981,1023,1023];
TGC.rangeMax = P.rxDepth;
TGC.Waveform = computeTGCWaveform(TGC);

%% Specify Receive structure arrays
%   endDepth - add additional acquisition depth to account for some channels
%              having longer path lengths.
%   InputFilter - The same coefficients are used for all channels. The
%              coefficients below give a broad bandwidth bandpass filter.
maxAcqLength = sqrt(P.rxDepth^2 + (Trans.numelements*Trans.spacing)^2) - P.rxStart;
wlsPer128 = 128/(P.spw*2); % wavelengths in 128 samples for 4 samplesPerWave
Receive = repmat(struct('Apod', ones(1,128), ...
						'aperture',1, ...
						'startDepth', P.rxStart, ...
						'endDepth', P.rxStart + wlsPer128*ceil(maxAcqLength/wlsPer128), ...
						'TGC', 1, ...
						'bufnum', 1, ...
						'framenum', 1, ...
						'acqNum', 1, ...
						'sampleMode', P.sampleMode, ...
						'mode', 0, ...
						'callMediaFunc', 0),1, ...
						P.na*Resource.RcvBuffer(1).numFrames + ...
						P.na*Resource.RcvBuffer(2).numFrames);
% - Set event specific Receive attributes for each frame.
% Real-time imaging loop
for i = 1:Resource.RcvBuffer(1).numFrames
	offset = (i-1) * P.na;
	Receive(offset+1).callMediaFunc = 1;
	for n = 1:P.na
		Receive(offset+n).framenum = i;
		Receive(offset+n).aperture = apers(n);
		Receive(offset+n).acqNum = n;
	end
end

% Data saving loop
for i = 1:Resource.RcvBuffer(2).numFrames
	offset = (i-1) * P.na + P.na*Resource.RcvBuffer(1).numFrames;
	Receive(offset+1).callMediaFunc = 1;
	for n = 1:P.na
		Receive(offset+n).bufnum = 2;
		Receive(offset+n).framenum = i;
		Receive(offset+n).aperture = apers(n);
		Receive(offset+n).acqNum = n;
	end
end


%% Specify Process structure array.
% External function definition
import vsv.seq.function.ExFunctionDef;
EF(1).Function = ExFunctionDef('beamformRawData', @beamformRawData);
EF(2).Function = ExFunctionDef('saveBuffers', @saveBuffers);
EF(3).Function = ExFunctionDef('resetStartEvent', @resetStartEvent);

Process(1).classname = 'External';
Process(1).method = 'beamformRawData';
Process(1).Parameters = {'srcbuffer','receive', ... % buffer to process
						 'srcbufnum',1, ... 		% live buffer
						 'srcframenum',-1, ... 		% most recent frame
						 'dstbuffer','none'}; 		% no output buffer

Process(2).classname = 'External';
Process(2).method = 'saveBuffers';
Process(2).Parameters = {'srcbuffer','receive', ...	% buffer to process
						 'srcbufnum',2, ... 		% save buffer
						 'srcframenum',0, ... 		% all frames
						 'dstbuffer','none'}; 		% no output buffer

Process(3).classname = 'External';
Process(3).method = 'resetStartEvent';
Process(3).Parameters = {'srcbuffer','none', ... 	% no input buffer
						 'dstbuffer','none'}; 		% no output buffer
					 
%% Specify SeqControl structure arrays.
% Compute the time between the last acquisition and the next frame in usec
pulseTTNA = 1e6 / P.acqPRF;
frameTTNA = 1e6 / P.acqFPS - (pulseTTNA * (P.na - 1));
fprintf('Acquisition frame rate: %d fps.\n', P.acqFPS);
fprintf('Pulse reptition frequency: %.1fkHz.\n', P.acqPRF*1e-3);
if frameTTNA < 10  % If < 10 usec between frames
	error('Timing for frame is too tight. Try increasing PRF or decreasing FPS.')
end
maxAcqLength = sqrt(P.rxDepth^2 + (Trans.numelements*Trans.spacing)^2) - P.rxStart;
maxPossiblePRF = 1 / (wvlnToMm(maxAcqLength)*1e-3 / Resource.Parameters.speedOfSound);
if maxPossiblePRF < P.acqPRF
    error('Requested PRF %4.1f kHz is too high. Maximum possible is %4.1f kHz.', P.acqPRF, maxPossiblePRF)
end


% "SeqControlArray" is a convenient wrapper class to construct SeqControl.
SCA = SeqControlArray();  % Defined in vsxBF/helper_classes/SeqControlArray.m
% Predefine some reusable SeqControl entries with addSC(cmd, arg, cond, label)
SCA.addSC('timeToNextAcq', pulseTTNA, [], 'pTTNA');  % Pulse time to next acq
SCA.addSC('timeToNextAcq', frameTTNA, [], 'fTTNA');  % Frame time to next acq
SCA.addSC('returnToMatlab', [], [], 'return');  % Return to MATLAB
SCA.addSC('jump', 1, [], 'jump');  % Jump back to beginning

%% Specify Event structure arrays.
% "EventArray" is a convenient wrapper class to construct Event.
EA = EventArray;  % Defined in vsxBF/helper_classes/EventArray.m

% Live imaging events
liveEvent = EA.nextIdx();  % The live loop starts on the next Event's idx
frameSize = P.na;  % Number of acquisitions in a frame
for i = 1:Resource.RcvBuffer(1).numFrames
	% Get all TX/RX events for the i-th frame
	offset = (i-1)*frameSize;
	% Add a whole frame with the same fields, incrementing only tx and rcv
	EA.addTxRxFrame(P.na, 0, offset, 0, 0, SCA.get('pTTNA'), 'Transmit');
	% Change the last Event's TTNA to frameTTNA and transfer data to host
	SCA.addSC('transferToHost', [], [], sprintf('Frame %d transfer', i));
	EA.modifyPrevSeqControl([SCA.get('fTTNA'), SCA.currIdx()]);
	% Add beamforming events
	EA.addEvent(0, 0, 0, 1, SCA.get('return'), 'Beamform custom');
end
% Add a jump to beginning event
EA.addEvent(0, 0, 0, 0, SCA.get('jump'), 'Jump to beginning');

% Save loop events
saveEvent = EA.nextIdx();  % The save loop starts on the next Event's idx
for i = 1:Resource.RcvBuffer(2).numFrames
	% Get all TX/RX events for the i-th frame
	offset = (Resource.RcvBuffer(1).numFrames + i-1)*frameSize;
	% Add a whole frame with the same fields, incrementing only tx and rcv
	EA.addTxRxFrame(P.na, 0, offset, 0, 0, SCA.get('pTTNA'), 'Transmit');
	% Change the last Event's TTNA to frameTTNA and transfer data to host
	SCA.addSC('transferToHost', [], [], sprintf('Frame %d transfer', i));
	EA.modifyPrevSeqControl([SCA.get('fTTNA'), SCA.currIdx()]);
end
% Add SeqControl and Event that waits on the final transferToHost
SCA.addSC('waitForTransferComplete', SCA.currIdx(), [], 'Wait For Last Transfer');
EA.addEvent(0, 0, 0, 0, [SCA.get('sTTNA'), SCA.currIdx()], 'Wait for data');
% Add a save event and jump back to beginning event
EA.addEvent(0, 0, 0, 2, SCA.get('return'), 'Save raw data');
EA.addEvent(0, 0, 0, 3, SCA.get('return'), 'Reset StartEvent');
EA.addEvent(0, 0, 0, 0, SCA.get('jump'), 'Jump to beginning');

%% User specified UI Control Elements
% "UIArray" is a convenient wrapper class to construct Event.
UIA = UIArray;  % Defined in vsxBF/helper_classes/UIArray.m
UIA.addSlider('UserB8', 'Start Depth', 0, 40, P.rxStartMm, [5 10], '%3.0f', @RxStartChangeCallback);
UIA.addSlider('UserB7', 'End Depth', 0, 40, P.rxDepthMm, [5 10], '%3.0f', @RxDepthChangeCallback);
UIA.addSlider('UserB6', 'F-Number', .1, 5, R.fnum, [.5 1], '%4.2f', @FNumberChangeCallback);
UIA.addToggle('UserB5', 'Median Filter', @MedianFilterCallback);
UIA.addButton('UserB4', 'Save Raw Data', @SaveDataCallback);
UIA.addSlider('UserC8', 'FOV Width', 5, 30, R.fovWidthMm, [5 10], '%3.0f', @FovWidthChangeCallback);
UIA.addSlider('UserC7', 'Output SpWl', .5, 4, R.outspwl, [.25 .5], '%.2f', @OutSpwlChangeCallback);

%% Grab the actual SeqControl, Event, and UI structures from their *Array classes
SeqControl = SCA.data;
Event = EA.data;
UI = UIA.data;

%% Specify factor for converting sequenceRate to frameRate.
frameRateFactor = 1;

%% Save all the structures to a .mat file.
curdir = pwd;
sname = [pwd '/MatFiles/' P.configName];
disp(['filename = ''' sname '''; VSX'])
save(sname);
filename = sname;
VSX;

return

%% External functions
function saveBuffers(full_buf)
	global b_buf ax lat frameNum fps
	if isempty(fps), clear fps, end

	% Create the save directory if it does not already exist
	saveDir = evalin('base','saveDir');
	if ~exist(saveDir,'dir'); mkdir(saveDir); end

	% Use namingScheme defined in vsxBF/functions/generateFilename.m
	namingScheme = evalin('base','namingScheme');
	fname = sprintf('%s/%s_rawbuf.mat', saveDir, generateFilename(namingScheme));
	bname = sprintf('%s/%s_rawbuf.bin', saveDir, generateFilename(namingScheme));

	% Get key structs needed for later post-processing
	Resource = evalin('base', 'Resource');
	Trans	 = evalin('base', 'Trans');
	TW		 = evalin('base', 'TW');
	TX		 = evalin('base', 'TX');
	Receive  = evalin('base', 'Receive');
	P = evalin('base','P');
	R = evalin('base','R');
	% Also grab the TxVoltage at the time of acquisition
	P.TxVoltage = get(findobj(0,'Tag','hv1Value'),'String');

	% Save the raw full buffer as a binary file for faster storage.
	% It is highly recommended that this data be compressed later to reduce its size.
	tic; fprintf('Saving raw data to %s...\n', bname)
	fid = fopen(bname, 'wb');
	full_buf_count = fwrite(fid, full_buf, 'int16');
	full_buf_size = size(full_buf);
	fclose(fid); clear fid;
	clear full_buf

	% Save all other variables to a .mat file (-v6 flag for speed).
	fprintf('Saving workspace variables to %s...\n', fname)
	save(fname, '-v6', '-regexp','^(?!(full_buf)$).')  % All vars except full_buf
	fprintf('Took %.3f seconds to save data.\n', toc)

	% If using HIPAA compliant mode, scramble the timestamps
	if strcmp(namingScheme, 'anonymous')
		scrambleTimestamps(fname);
		scrambleTimestamps(bname);
	end
end

function beamformRawData(rcvbuf)
	persistent h_bimg h_ax h_fps nsamps nxmits
	persistent t1 t2
	persistent b_max bufSize
	global b_buf ax lat fps frameNum

	% On first invocation, initialize
	if ~evalin('base','R.initialized')
		% Get user parameters from base workspace
		R = evalin('base','R');
		P = evalin('base','P');	
		P.nxmits = P.na;
		% Grab relevant data structures from base workspace
		Resource = evalin('base','Resource');
		Receive	 = evalin('base','Receive');
		TX		 = evalin('base','TX');
		Trans	 = evalin('base','Trans');
		TW		 = evalin('base','TW');
		% Call the standalone processing function
		[lat, ax] = process_L12_3v_bmode_synap_tf(P, R, Resource, Receive, TX, Trans, TW);
		bimg = process_L12_3v_bmode_synap_tf(rcvbuf);
		bimg = 10*log10(bimg);  % Network outputs magnitude squared
		bimg(length(ax)+1:end,:) = []; % Remove extra rows at the end
		% Initialize cine-loop normalization vector, timing
		bufSize = 30; % Real-time cine-loop buffer size
		b_buf = zeros(size(bimg,1),size(bimg,2),bufSize,'single');
		b_max = max(bimg(:))*ones(bufSize,1,'single');
		% Subtract off max value
		bimg = bimg - max(bimg(:));		   

		% Plot for the first time
		figure(1001); clf
		aspectRatio = (lat(end)-lat(1)) / (ax(end)-ax(1));
		maxHeight = 800; maxWidth = 600;	
		if aspectRatio > maxWidth/maxHeight, width = maxWidth; height = maxWidth/aspectRatio;
		else, height = maxHeight; width = maxHeight*aspectRatio;
		end
		set(gcf,'Position',[100 350 width height]);
		colormap gray
		h_ax = gca;
		h_bimg = imagesc(lat, ax, bimg, R.brange); axis image
		set(h_ax,'Position',[0.05 0.05 0.9 0.9]);
		xlabel('Azimuth (mm)')
		ylabel('Depth (mm)')
		title('B-mode')
		colorbar
		h_fps = annotation('textbox',[0.45 0.02 0.1 0.05], 'String',sprintf('%2.0f fps',fps), ...
			'FontWeight', 'bold', 'FontSize', 14, 'Color','k','EdgeColor','none',...
			'HorizontalAlignment','center');
		frameNum = 1;
		t1 = tic-5e7;
		fps = 1;

		% Mark as initialized
		evalin('base','R.initialized = 1;');
	else
		% Execute GPU BF
		bimg = process_L12_3v_bmode_synap_tf(rcvbuf);
		bimg = 10*log10(bimg);  % Network outputs magnitude squared
		bimg(length(ax)+1:end,:) = []; % Remove extra rows at the end
		b_max(frameNum) = max(bimg(:));
		% Use a history of maximum values to normalize
		% Use fps/2 frames (i.e. half a second)
		maxHistory = mod(frameNum+(-round(fps/2):0)-1, bufSize) + 1;
		bimg = bimg - mean(b_max(maxHistory));
		% Store in buffer
		b_buf(:,:,frameNum) = bimg;
		% If requested, median filter the image
		if evalin('base', 'R.useMedianFilter')
			bimg = medfilt2(bimg, [3 3]);
		end

		% Update image
		set(h_bimg, 'CData', bimg);
		if evalin('base', 'updatedRange')
			set(h_ax, 'CLim', R.brange);
			assignin('base','updatedRange',false);
		end
		% Update counters for fps and buffer
		frameNum = mod(frameNum,bufSize)+1;
		% Compute fps
		if mod(frameNum, 10) == 0
			t2 = toc(t1);
			fps = 10/t2;
			t1 = tic;
			set(h_fps,'String',sprintf(' %.0f fps',fps));
		end
	end
end

function resetStartEvent()
	% Set the new "startEvent" to liveEvent, the first Event of the live loop.
	% This is necessary because other callbacks may change the startEvent.
	% For instance, the save button sets startEvent = saveEvent; causing all
	% subsequent freeze/unfreeze events to start in the save loop.
	% This function should be called to restore startEvent = liveEvent.
	str = 'liveEvent';
	Control = evalin('base', 'Control');
	Control(1).Command = 'set';  % No need for set&Run here
	Control(1).Parameters = {'Parameters', 1, 'startEvent', evalin('base', str)};
	evalin('base', ['Resource.Parameters.startEvent = ' str ';']);
	assignin('base', 'Control', Control);
end

function RxStartChangeCallback(~, ~, UIValue)
	simMode = evalin('base','Resource.Parameters.simulateMode');
	% No range change if in simulate mode 2.
	if simMode == 2
		set(hObject,'Value',evalin('base','P.rxStartMm'));
		return
	end
	P = evalin('base','P');
	P.rxStartMm = UIValue;
	% Make sure that P.rxStartMm is at least 5mm less than P.rxDepthMm
	if P.rxStartMm > P.rxDepthMm-5
		P.rxStartMm = P.rxDepthMm-5;
	end
	% If P.rxStartMm has changed, update control structures
	if P.rxStartMm ~= evalin('base','P.rxStartMm')
		Trans = evalin('base','Trans');
		mmToWvln = evalin('base','mmToWvln');
		P.rxStart = mmToWvln(P.rxStartMm);
		Receive = evalin('base', 'Receive');
		for i = 1:size(Receive,2)
			Receive(i).startDepth = P.rxStart;
		end
		Control = evalin('base','Control');
		Control.Command = 'update&Run';
		Control.Parameters = {'PData','Receive','Recon','DisplayWindow','ImageBuffer'};
		clear L12_3v_umi_slac
		evalin('base','R.initialized = false;');
		assignin('base','P',P);
		assignin('base','Receive',Receive);
		assignin('base','Control', Control);
	end
end

function RxDepthChangeCallback(~, ~, UIValue)
	simMode = evalin('base','Resource.Parameters.simulateMode');
	% No range change if in simulate mode 2.
	if simMode == 2
		set(hObject,'Value',evalin('base','P.rxDepthMm'));
		return
	end
	P = evalin('base','P');
	P.rxDepthMm = UIValue;
	% Make sure that P.rxDepthMm is at least 5mm greater than P.rxStartMm
	if P.rxDepthMm < P.rxStartMm+5
		P.rxDepthMm = P.rxStartMm+5;
	end
	% If P.rxDepthMm has changed, update control structures
	if P.rxDepthMm ~= evalin('base','P.rxDepthMm')
		Trans = evalin('base','Trans');
		mmToWvln = evalin('base','mmToWvln');
		P.rxDepth = mmToWvln(P.rxDepthMm);
		Receive = evalin('base', 'Receive');
		maxAcqLength = sqrt(P.rxDepth^2 + (Trans.numelements*Trans.spacing)^2) - P.rxStart;
		wlsPer128 = 128/(P.spw*2); % wavelengths in 128 samples for 4 samplesPerWave
		for i = 1:size(Receive,2)
			Receive(i).endDepth = P.rxStart + wlsPer128*ceil(maxAcqLength/wlsPer128);
		end
		TGC = evalin('base','TGC');
		TGC.rangeMax = P.rxDepth;
		TGC.Waveform = computeTGCWaveform(TGC);
		Control = evalin('base','Control');
		Control.Command = 'update&Run';
		Control.Parameters = {'PData','Receive','Recon','DisplayWindow','ImageBuffer'};
		clear L12_3v_umi_slac
		evalin('base','R.initialized = false;')
		assignin('base','P',P);
		assignin('base','Receive',Receive);
		assignin('base','TGC',TGC);
		assignin('base','Control',Control);
	end
end

function FNumberChangeCallback(~, ~, UIValue)
	if UIValue ~= evalin('base','R.fnum')
		R = evalin('base','R');
		R.fnum = UIValue;
		R.initialized = false;
		assignin('base','R',R);
	end
end

function SaveDataCallback(~, ~, UIValue)
	% Set the new "startEvent" to saveEvent, the first Event of the save loop
	str = 'saveEvent';
	Control = evalin('base', 'Control');
	Control(1).Command = 'set&Run';
	Control(1).Parameters = {'Parameters', 1, 'startEvent', evalin('base', str)};
	evalin('base', ['Resource.Parameters.startEvent = ' str ';']);
	assignin('base', 'Control', Control);
end

function FovWidthChangeCallback(~, ~, UIValue)
	R = evalin('base','R');
	R.fovWidthMm = UIValue;
	if R.fovWidthMm ~= evalin('base','R.fovWidthMm')
		Trans = evalin('base','Trans');
		mmToWvln = evalin('base','mmToWvln');
		R.fovWidth = mmToWvln(R.fovWidthMm);
		R.initialized = false;
		assignin('base','R',R);
	end
end

function OutSpwlChangeCallback(~, ~, UIValue)
	R = evalin('base','R');
	R.outspwl = UIValue;
	if R.outspwl ~= evalin('base','R.outspwl')
		R.initialized = false;
		assignin('base','R',R);
	end
end

function MedianFilterCallback(~, ~, UIValue)
	R = evalin('base','R');
	R.useMedianFilter = UIValue;
	assignin('base','R',R);
end
