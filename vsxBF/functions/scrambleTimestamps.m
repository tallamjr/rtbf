% Scramble the timestamp metadata.
% Useful for acquiring anonymized HIPAA compliant data.
% Thanks to https://www.mathworks.com/matlabcentral/answers/452185
function scrambleTimestamps(fname)
    if ispc
        modifiedTime = '$(Get-Date \"03/24/2005 21:30\")';
        for attrib = {'lastwritetime', 'creationtime', 'lastaccesstime'}
            eval(sprintf('!powershell $(Get-Item %s).%s=%s', fname, attrib{:}, modifiedTime));
        end
    else
        modifiedTime = '200503242130';
        eval(sprintf('!touch -t %s %s', modifiedTime, fname));
    end
end