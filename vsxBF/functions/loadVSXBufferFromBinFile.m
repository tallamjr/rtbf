% loadVSXBufferFromBinFile.m
% Load data saved using the save button in the vsxBF example scripts.
% If the MATLAB structs and raw data are saved as:
%     my/data/folder/file1.mat
%     my/data/folder/file1.bin
% this function should be invoked as
%     full_buf = loadVSXBufferFromBinFile('my/data/folder/file1');
function full_buf = loadVSXBufferFromBinFile(filePref)

	% File names
	bfile = sprintf('%s.bin', filePref);
	mfile = sprintf('%s.mat', filePref);

	% Get data size from MATLAB file
	load(mfile, 'full_buf_count', 'full_buf_size')

	% Load VSX buffer from binary file
	fid = fopen(bfile, 'r');
	full_buf = fread(fid, full_buf_count, 'int16=>int16');
	full_buf = reshape(full_buf, full_buf_size);
	fclose(fid);

end

