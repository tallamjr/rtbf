% Automatically generate filenames that can be sorted in ascending order.
% Can use the current date and time, or use a scrambled HIPAA compliant number.
function fname = generateFilename(namingScheme)
    if strcmp(namingScheme, 'datestr')
		% 'datestr' returns the exact date and time of acquisition, allowing us to save
		% phantom and animal data with precise recordkeeping. This mode should not be
		% used for clinical acquisitions.
		fname = datestr(now,'yyyymmdd_HHMMSS');
	elseif strcmp(namingScheme, 'anonymous')
		% 'cputime' returns the elapsed CPU time since the start of the MATLAB process.
		% By naming files by cputime, we avoid storing any date or time information
		% (i.e. protected health information) explicitly, preserving time-ordering of
		% acquisitions anonymously while remaining HIPAA compliant in the US.
		fname = num2str(round(cputime * 1e3));
	else
		error('Unknown naming scheme. Choose datestr or anonymous.')
	end
end
