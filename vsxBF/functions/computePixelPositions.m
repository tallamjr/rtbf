% Compute the pixel positions in wavelengths
function pixPos = computePixelPositions(beamOri, beamDir, nrows, spwl)
ncols = size(beamOri,1);
pixPos = zeros(nrows, ncols, 3, 'single');
depthInWL = (1:nrows)' / spwl;
for C = 1:ncols
	pixPos(:,C,1) = beamOri(C,1) + depthInWL * sin(beamDir(C,1)) * cos(beamDir(C,2));	% x-coordinate
	pixPos(:,C,2) = beamOri(C,2) + depthInWL * sin(beamDir(C,2));						% y-coordinate
	pixPos(:,C,3) = beamOri(C,3) + depthInWL * cos(beamDir(C,1)) * cos(beamDir(C,2));	% z-coordinate
end
end

