/**
 @file vsxBF/utils/VSXDataFormatter.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-03-13

Copyright 2019 Dongwoon Hyun

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "VSXDataFormatter.cuh"

namespace rtbf {
namespace kernels = VSXDataFormatterKernels;

template <typename T>
VSXDataFormatter<T>::VSXDataFormatter() {
  this->resetDataProcessor();
  reset();
}
template <typename T>
VSXDataFormatter<T>::~VSXDataFormatter() {
  reset();
}
template <typename T>
void VSXDataFormatter<T>::reset() {
  if (this->isInit) this->printMsg("Clearing object.");
  CCE(cudaSetDevice(this->devID));

  // VSXDataFormatter members
  nsamps = 0;
  nxmits = 0;
  nchans = 0;
  nacqch = 0;
  npulses = 0;
  nframes = 0;
  mode = VSXSampleMode::HILBERT;
  in.reset();
  chanmap.reset();

  // Base class members
  this->resetDataProcessor();
  strcpy(this->moniker, "VSXDataFormatter");
}

// Initialization function
template <typename T>
void VSXDataFormatter<T>::initialize(int numSamples, int numTransmits,
                                     int numChannels, int numAcqChannels,
                                     int numFrames, VSXSampleMode sampleMode,
                                     int numPulsesToSum, int *h_channelMapping,
                                     int deviceID, cudaStream_t cudaStream,
                                     int verbosity) {
  // If previously initialized, reset
  reset();

  // Populate parameters
  nsamps = numSamples / 2;  // Every two samples --> one IQ sample
  nxmits = numTransmits;
  nchans = numChannels;
  nacqch = numAcqChannels;
  npulses = numPulsesToSum;
  nframes = numFrames;
  mode = sampleMode;
  this->setDeviceID(deviceID);
  this->stream = cudaStream;
  this->verb = verbosity;
  this->printMsg("Initializing object.");

  int nsamps_tmp, nsamps_out;
  if (mode == VSXSampleMode::BS50BW) {
    this->printMsg("BS50BW", 2);
    nsamps_tmp = nsamps * 8;
    nsamps_out = nsamps;
  } else if (mode == VSXSampleMode::BS100BW) {
    this->printMsg("BS100BW", 2);
    nsamps_tmp = nsamps * 4;
    nsamps_out = nsamps;
  } else if (mode == VSXSampleMode::NS200BW) {
    this->printMsg("NS200BW", 2);
    nsamps_tmp = nsamps * 2;
    nsamps_out = nsamps;
  } else {
    this->printMsg("HILBERT", 2);
    nsamps_tmp = nsamps * 2;
    nsamps_out = nsamps * 2;
  }

  // Initialize data arrays on current GPU
  CCE(cudaSetDevice(this->devID));

  // Initialize input DataArray (store as short2 for easier processing)
  in.initialize(nsamps * npulses * nxmits * nframes, nacqch, 1, 1, deviceID,
                "VSXDataFormatter (input)", this->verb);

  // Initialize a temporary DataArray for computations (only need 1 frame)
  // Input short2 (I,Q) will be assigned into subsequent samples as (I,0),
  // (0,Q)
  tmp.initialize(nsamps_tmp, nxmits, nchans, 1, this->devID,
                 "VSXDataFormatter (temporary)", this->verb);

  // Initialize output DataArray
  this->out.initialize(nsamps_out, nxmits, nchans, nframes, this->devID,
                       "VSXDataFormatter (output)", this->verb);

  // Store the channel mapping as an array on the GPU if it is used
  if (h_channelMapping != nullptr) {
    chanmap.initialize(nacqch * nxmits, 1, 1, 1, this->devID,
                       "VSXDataFormatter (channel map)", this->verb);
    chanmap.copyToGPU(h_channelMapping);
    this->printMsg("Using custom channel mapping.", this->verb);
  }

  // The temporary data will be processed as FFT->BPF->IFFT
  CCE(cufftCreate(&fftplan));
  int x = tmp.getNx();  // Number of rows of valid data
  int p = tmp.getNp();  // Number of rows of data pitch
  int y = tmp.getNy();  // Number of columns of data
  int c = tmp.getNc();  // Number of channels in data
  CCE(cufftPlanMany(&fftplan, 1, &x, &p, 1, p, &p, 1, p, CUFFT_C2C, y * c));
  CCE(cufftSetStream(fftplan, this->stream));

  // Get strides of data based on the data ordering (hard coded for now)
  VSXDataOrder dataOrder = VSXDataOrder::PXF;
  getPitches(&ppitch, &xpitch, &fpitch, dataOrder);

  // Mark as initialized
  this->isInit = true;
}

template <typename T>
void VSXDataFormatter<T>::getPitches(int *ppitch, int *xpitch, int *fpitch,
                                     VSXDataOrder order) {
  int ns = nsamps;   // Number of samples
  int np = npulses;  // Number of pulses to sum
  int nx = nxmits;   // Number of transmission locations
  int nf = nframes;  // Number of Doppler frames, i.e., ensemble length
  // Compute strides between consecutive pulses, transmits, and Doppler frames
  if (order == VSXDataOrder::PXF) {
    *ppitch = ns;
    *xpitch = ns * np;
    *fpitch = ns * np * nx;
  } else if (order == VSXDataOrder::PFX) {
    *ppitch = ns;
    *fpitch = ns * np;
    *xpitch = ns * np * nf;
  } else if (order == VSXDataOrder::XPF) {
    *xpitch = ns;
    *ppitch = ns * nx;
    *fpitch = ns * nx * np;
  } else if (order == VSXDataOrder::FPX) {
    *fpitch = ns;
    *ppitch = ns * nf;
    *xpitch = ns * nf * np;
  } else if (order == VSXDataOrder::XFP) {
    *xpitch = ns;
    *fpitch = ns * nx;
    *ppitch = ns * nx * nf;
  } else if (order == VSXDataOrder::FXP) {
    *fpitch = ns;
    *xpitch = ns * nf;
    *ppitch = ns * nf * nx;
  }
}

// This function copies the data from the host to the GPU and selects
// the proper CUDA kernel to execute
template <typename T>
void VSXDataFormatter<T>::formatRawVSXData(short *h_raw, int pitchInSamps) {
  if (!this->isInit) {
    this->printErr("Object not initialized.", __FILE__, __LINE__);
    return;
  }

  // Copy data from host to the input GPU array
  in.copyToGPUAsync((short2 *)h_raw, this->stream,
                    sizeof(short) * pitchInSamps);

  // Get data pointers and memory pitch
  short2 *idata = in.getDataPtr();   // Input data pointer
  float2 *tdata = tmp.getDataPtr();  // Temporary data pointer

  // Loop through frames
  for (int f = 0; f < nframes; f++) {
    // Unwrap the raw data into the tmp DataArray
    tmp.resetToZerosAsync(this->stream);  // Set all values to zero

    // Current frame corresponds to idata incremented by f*fpitch
    unwrapData(idata + f * fpitch);

    // FFT the data
    CCE(cufftExecC2C(fftplan, tdata, tdata, CUFFT_FORWARD));

    // Bandpass filter the data according to the VSXSampleMode
    filterData();

    // IFFT the data back
    CCE(cufftExecC2C(fftplan, tdata, tdata, CUFFT_INVERSE));

    // Finally, downsample, cast, and store data in output DataArray
    downsampleData(this->out.getFramePtr(f));
  }
}

template <typename T>
void VSXDataFormatter<T>::unwrapData(short2 *idata) {
  // Create computation grid
  dim3 B(256, 1, 1);
  dim3 G((nsamps - 1) / B.x + 1, (nxmits - 1) / B.y + 1,
         (nacqch - 1) / B.z + 1);

  // If a channel map is given, use it.
  // TODO: Needs testing
  int *cmap = chanmap.isInitialized() ? chanmap.getDataPtr() : nullptr;
  int ipitch = in.getNp();
  int tpitch = tmp.getNp();
  float2 *tdata = tmp.getDataPtr();

  // First, copy raw data into output array with proper transpositions
  if (mode == VSXSampleMode::BS50BW) {  // 1 sample per cycle
    kernels::unwrapIQ<1><<<G, B, 0, this->stream>>>(
        nsamps, npulses, nxmits, nacqch, nchans, ppitch, xpitch, idata, ipitch,
        tdata, tpitch, cmap);
  } else if (mode == VSXSampleMode::BS100BW) {  // 2 samples per cycle
    kernels::unwrapIQ<2><<<G, B, 0, this->stream>>>(
        nsamps, npulses, nxmits, nacqch, nchans, ppitch, xpitch, idata, ipitch,
        tdata, tpitch, cmap);
  } else if (mode == VSXSampleMode::NS200BW) {  // 4 samples per cycle
    kernels::unwrapIQ<4><<<G, B, 0, this->stream>>>(
        nsamps, npulses, nxmits, nacqch, nchans, ppitch, xpitch, idata, ipitch,
        tdata, tpitch, cmap);
  } else if (mode == VSXSampleMode::HILBERT) {  // Hilbert Transform version
    kernels::unwrapIQ<-1><<<G, B, 0, this->stream>>>(
        nsamps, npulses, nxmits, nacqch, nchans, ppitch, xpitch, idata, ipitch,
        tdata, tpitch, cmap);
  }
}
template <typename T>
void VSXDataFormatter<T>::filterData() {
  // Create computation grid
  int nsamps_tmp = tmp.getNx();
  dim3 B(256, 1, 1);
  dim3 G((nsamps_tmp - 1) / B.x + 1, (nxmits - 1) / B.y + 1,
         (nchans - 1) / B.z + 1);

  float2 *tdata = tmp.getDataPtr();
  int tpitch = tmp.getNp();

  // First, copy raw data into output array with proper transpositions
  if (mode == VSXSampleMode::BS50BW) {  // 1 sample per cycle
    kernels::bandpassFilter<1>
        <<<G, B, 0, this->stream>>>(nsamps_tmp, nxmits, nchans, tdata, tpitch);
  } else if (mode == VSXSampleMode::BS100BW) {  // 2 samples per cycle
    kernels::bandpassFilter<2>
        <<<G, B, 0, this->stream>>>(nsamps_tmp, nxmits, nchans, tdata, tpitch);
  } else if (mode == VSXSampleMode::NS200BW) {  // 4 samples per cycle
    kernels::bandpassFilter<4>
        <<<G, B, 0, this->stream>>>(nsamps_tmp, nxmits, nchans, tdata, tpitch);
  } else if (mode == VSXSampleMode::HILBERT) {  // Hilbert Transform version
    kernels::bandpassFilter<-1>
        <<<G, B, 0, this->stream>>>(nsamps_tmp, nxmits, nchans, tdata, tpitch);
  }
}
template <typename T>
void VSXDataFormatter<T>::downsampleData(T *odata) {
  int nsamps_out = this->out.getNx();
  // Create computation grid
  dim3 B(256, 1, 1);
  dim3 G((nsamps_out - 1) / B.x + 1, (nxmits - 1) / B.y + 1,
         (nchans - 1) / B.z + 1);
  int tpitch = tmp.getNp();
  int opitch = this->out.getNp();
  float2 *tdata = tmp.getDataPtr();

  // Downsample the redundant resampled data to reduce data size.
  // Note: Hilbert Transform version does not need downsampling.
  if (mode == VSXSampleMode::BS50BW) {  // 1 sample per cycle
    kernels::downsampleIQ<T, 1><<<G, B, 0, this->stream>>>(
        nsamps_out, nxmits, nchans, tdata, tpitch, odata, opitch);
  } else if (mode == VSXSampleMode::BS100BW) {  // 2 samples per cycle
    kernels::downsampleIQ<T, 2><<<G, B, 0, this->stream>>>(
        nsamps_out, nxmits, nchans, tdata, tpitch, odata, opitch);
  } else if (mode == VSXSampleMode::NS200BW) {  // 4 samples per cycle
    kernels::downsampleIQ<T, 4><<<G, B, 0, this->stream>>>(
        nsamps_out, nxmits, nchans, tdata, tpitch, odata, opitch);
  } else if (mode == VSXSampleMode::HILBERT) {  // Hilbert Transform version
    kernels::downsampleIQ<T, -1><<<G, B, 0, this->stream>>>(
        nsamps_out, nxmits, nchans, tdata, tpitch, odata, opitch);
  }
}

template <int spc>
__global__ void kernels::unwrapIQ(int nsamps, int npulses, int nxmits,
                                  int nacqch, int nchans, int ppitch,
                                  int xpitch, short2 *idata, int ipitch,
                                  float2 *tdata, int tpitch, int *cmap) {
  // Determine information about the current thread
  int samp = blockIdx.x * blockDim.x + threadIdx.x;
  int xmit = blockIdx.y * blockDim.y + threadIdx.y;
  int acqc = blockIdx.z * blockDim.z + threadIdx.z;

  // Only execute kernel if valid
  if (samp < nsamps && xmit < nxmits && acqc < nacqch) {
    // Output channel index; destination of current data
    int chan = cmap == nullptr ? acqc : cmap[acqc + nacqch * xmit];
    // Advance input and output data pointers to current sample
    // For spc==4, idata[samp] --> (tdata[2*samp+0].x, tdata[2*samp+1].y)
    // For spc==2, idata[samp] --> (tdata[4*samp+0].x, tdata[4*samp+1].y)
    // For spc==1, idata[samp] --> (tdata[8*samp+0].x, tdata[8*samp+1].y)
    idata += xmit * xpitch + acqc * ipitch;
    tdata += tpitch * (xmit + nxmits * chan);
    float sumI = 0.f;
    float sumQ = 0.f;
    // Loop through summed pulses (e.g. pulse inversion harmonic imaging)
    for (int p = 0; p < npulses; p++) {
      // Accumulate values
      if (spc == 4 && samp & 1) {  // NS200BW: Negate odd samples
        sumI -= (float)idata[samp + p * ppitch].x;  // Grab I component
        sumQ -= (float)idata[samp + p * ppitch].y;  // Grab Q component
      } else {
        sumI += (float)idata[samp + p * ppitch].x;  // Grab I component
        sumQ += (float)idata[samp + p * ppitch].y;  // Grab Q component
      }
    }
    // Write the sums for sample pairs into tdata
    if (spc == -1) {  // VSXSampleMode::HILBERT
      // In ordinary RF sampling mode, treat data as I, I, I, I, I, ...
      tdata[samp * 2 + 0].x = sumI;  // I
      tdata[samp * 2 + 1].x = sumQ;  // I
    } else {
      // In direct IQ sampling mode, treat data as I, Q, I, Q, I, ...
      tdata[samp * 8 / spc + 0].x = sumI;   // I
      tdata[samp * 8 / spc + 1].y = -sumQ;  // Q
    }
  }
}

/** @brief Bandpass filtering for anti-aliasing the upsampled data.
Verasonics data is already demodulated. The upsampling procedure in
unwrapData results in an aliased spectrum. This kernel applies an
anti-aliasing bandpass filter whose band depends on the samples per cycle of
the VSXSampleMode.
*/
template <int spc>
__global__ void kernels::bandpassFilter(int nsamps, int nxmits, int nchans,
                                        float2 *tdata, int tpitch) {
  // Determine information about the current thread
  int samp = blockIdx.x * blockDim.x + threadIdx.x;
  int xmit = blockIdx.y * blockDim.y + threadIdx.y;
  int chan = blockIdx.z * blockDim.z + threadIdx.z;

  // Only execute kernel if valid
  if (samp < nsamps && xmit < nxmits && chan < nchans) {
    // Each frequency will be weighted according to the sampling mode
    float weight;
    if (spc == -1) {  // Hilbert Transform mode
      // In Hilbert transform mode, we zero out the negative part of the
      // frequency spectrum (i.e. when normalized frequency > 0.5).
      if (nsamps & 1) {  // Hilbert Transform (odd nsamps)
        if (samp == 0) {
          weight = 1.f;
        } else if (samp < (nsamps + 1) / 2) {
          weight = 2.f;
        } else {
          weight = 0.f;
        }
      } else {  // Hilbert Transform (even nsamps)
        if (samp == 0 || samp == nsamps / 2) {
          weight = 1.f;
        } else if (samp < nsamps / 2) {
          weight = 2.f;
        } else {
          weight = 0.f;
        }
      }
    } else {  // Direct IQ sampling mode
      // In direct IQ sampling mode, we just need to bandpass filter around
      // normalized frequency 0.5. The bandwidth is determined by the samples
      // per cycle (spc) set by the VSXSampleMode.
      float f = 1.f * samp / nsamps;  // Current thread's normalized frequency
      float bw = 1.f * spc / 8;       // Total bandwidth of valid signal
      if (f < bw / 2 || f >= 1.f - bw / 2) {
        weight = 1.f / bw / nsamps;
      } else {
        weight = 0.f;
      }
    }
    // Apply weights to current sample
    int idx = samp + tpitch * (xmit + nxmits * chan);
    tdata[idx] *= weight;
  }
}

/** @brief The final data is highly oversampled. Now that an anti-aliasing
 * bandpass filter has been applied, we can safely downsample the data without
 * loss of information.
 */
template <typename T, int spc>
__global__ void kernels::downsampleIQ(int nsamps_out, int nxmits, int nchans,
                                      float2 *tdata, int tpitch, T *odata,
                                      int opitch) {
  // Determine information about the current thread
  int samp = blockIdx.x * blockDim.x + threadIdx.x;
  int xmit = blockIdx.y * blockDim.y + threadIdx.y;
  int chan = blockIdx.z * blockDim.z + threadIdx.z;

  // Only execute kernel if valid
  if (samp < nsamps_out && xmit < nxmits && chan < nchans) {
    // Advance pointers to the current transmit and channel
    tdata += tpitch * (xmit + nxmits * chan);
    odata += opitch * (xmit + nxmits * chan);
    // Store the oversampled float2 tdata into the type T odata
    if (spc == -1) {
      // For RF mode, keep all samples
      saveData2(odata, samp, tdata[samp]);  // saveData2 casts to T
    } else {
      // For spc=={4, 2, 1}, downsample by a factor of {2, 4, 8}
      saveData2(odata, samp, tdata[samp * 8 / spc]);  // saveData2 casts to T
    }
  }
}

// Explicitly instantiate template for short2, float2 outputs
template class VSXDataFormatter<short2>;
template class VSXDataFormatter<float2>;
}  // namespace rtbf
