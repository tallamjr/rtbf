include_directories(
        ${Matlab_INCLUDE_DIRS})

add_library(vsxBF_utils STATIC
    VSXDataFormatter.cu
    VSXDataFormatter.cuh
    vsxBF_utils.cu
    vsxBF_utils.cuh
)

target_link_libraries(vsxBF_utils cufft)
