/**
 @file formatters/vsx_utils.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-07-01

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "vsxBF_utils.cuh"

namespace rtbf {

// Find the first GPU that can use texture objects (CC >= 3.0)
void getFirstAvailableGPU(int *gpuID) {
  int nDevices;
  CCE(cudaGetDeviceCount(&nDevices));
  for (int i = 0; i < nDevices; i++) {
    cudaDeviceProp prop;
    CCE(cudaGetDeviceProperties(&prop, i));
    if (prop.major >= 3) {
      *gpuID = i;
      break;
    }
  }
}

// Get a requried field from a struct
mxArray *getRequiredField(const mxArray *R, const char *field) {
  if (!mxIsStruct(R)) mexErrMsgTxt("Input must be a structure.");
  mxArray *A = mxGetField(R, 0, field);
  if (A == NULL) {
    char msg[256];
    sprintf(msg, "Required field %s is missing.", field);
    mexErrMsgTxt(msg);
  }
  return A;
}
// Get a required field from a struct with type checking for float
mxArray *getRequiredField(const mxArray *R, const char *field, float **dptr) {
  // First, get the required field
  mxArray *A = getRequiredField(R, field);
  // Additionally, check that the datatype is correct
  if (!mxIsEmpty(A) && mxGetClassID(A) != mxSINGLE_CLASS) {
    char msg[256];
    sprintf(msg, "%s is the wrong datatype. Expected single.", field);
    mexErrMsgTxt(msg);
  }
  return A;
}
// Get a required field from a struct with type checking for float
mxArray *getRequiredField(const mxArray *R, const char *field, float2 **dptr) {
  // First, get the required field
  mxArray *A = getRequiredField(R, field);
  // Additionally, check that the datatype is correct
  if (!mxIsEmpty(A) && mxGetClassID(A) != mxSINGLE_CLASS) {
    char msg[256];
    sprintf(msg, "%s is the wrong datatype. Expected single.", field);
    mexErrMsgTxt(msg);
  }
  return A;
}
// Get a field from a struct safely with type checking for int
mxArray *getRequiredField(const mxArray *R, const char *field, int **dptr) {
  // First, get the required field
  mxArray *A = getRequiredField(R, field);
  // Additionally, check that the datatype is correct
  if (!mxIsEmpty(A) && mxGetClassID(A) != mxINT32_CLASS) {
    char msg[256];
    sprintf(msg, "%s is the wrong datatype. Expected int32.", field);
    mexErrMsgTxt(msg);
  }
  return A;
}

// Get scalar from a structure
template <typename T>
void getReqScalar(const mxArray *R, const char *field, T *val) {
  mxArray *A = getRequiredField(R, field);
  *val = (T)mxGetScalar(A);
}

// Get optional scalar from a structure if it is a valid field. Do nothing
// otherwise.
template <typename T>
void getOptScalar(const mxArray *R, const char *field, T *val) {
  if (!mxIsStruct(R)) mexErrMsgTxt("Input must be a structure.");
  mxArray *A = mxGetField(R, 0, field);
  if (A != NULL) *val = (T)mxGetScalar(A);
}

// Explicitly instantiate getReqScalar and getOptScalar function
// templates
template void getReqScalar<>(const mxArray *, const char *, short *);
template void getReqScalar<>(const mxArray *, const char *, int *);
template void getReqScalar<>(const mxArray *, const char *, float *);
template void getReqScalar<>(const mxArray *, const char *, double *);
template void getOptScalar<>(const mxArray *, const char *, short *);
template void getOptScalar<>(const mxArray *, const char *, int *);
template void getOptScalar<>(const mxArray *, const char *, float *);
template void getOptScalar<>(const mxArray *, const char *, double *);

// Safely get delay and apodization tables with type and size error checking
void getDelayApodTables(const mxArray *R, const char *field, float **table,
                        int nrows, int ncols, int npages) {
  // Get field with existing checking and type checking
  mxArray *A = getRequiredField(R, field, table);
  // If the table is not specified, return nullptr
  if (mxIsEmpty(A)) {
    *table = nullptr;
    return;
  }
  // Check dimensions
  const mwSize *dims = mxGetDimensions(A);
  int d[3] = {1, 1, 1};
  for (int i = 0; i < mxGetNumberOfDimensions(A); i++) d[i] = dims[i];
  if (nrows != d[0] || ncols != d[1] || npages != d[2]) {
    char msg[256];
    sprintf(msg,
            "%s does not match input dimensions of (%d,%d,%d). Found (% d, % "
            "d, % d).",
            field, nrows, ncols, npages, d[0], d[1], d[2]);
    mexErrMsgTxt(msg);
  }
  *table = (float *)mxGetData(A);
}

// Safely get delay and apodization tables with type and size error checking
void getEnsembleFilter(const mxArray *R, const char *field, float **filt,
                       int *M, int *N) {
  // Get field with existing checking and type checking
  mxArray *A = getRequiredField(R, field, filt);
  // If the table is not specified, return nullptr
  if (mxIsEmpty(A)) {
    *filt = nullptr;
    *M = 0;
    *N = 0;
    return;
  }
  *filt = (float *)mxGetData(A);
  *M = mxGetM(A);  // Number of rows in filter matrix
  *N = mxGetN(A);  // Number of columns in filter matrix
}

void getFloat2Data(const mxArray *R, const char *field, float2 **data,
                   int nrows, int ncols, int npages) {
  // Get field with existing checking and type checking
  mxArray *A = getRequiredField(R, field, data);
  // If the data is not specified, return nullptr
  if (mxIsEmpty(A)) {
    *data = nullptr;
    return;
  }
  // Check dimensions
  const mwSize *dims = mxGetDimensions(A);
  int d[3] = {1, 1, 1};
  for (int i = 0; i < mxGetNumberOfDimensions(A); i++) d[i] = dims[i];
  if (nrows != d[0] || ncols != d[1] || npages != d[2]) {
    char msg[256];
    sprintf(msg,
            "%s does not match input dimensions of (%d,%d,%d). Found (% d, % "
            "d, % d).",
            field, nrows, ncols, npages, d[0], d[1], d[2]);
    mexErrMsgTxt(msg);
  }
  *data = (float2 *)mxGetComplexSingles(A);
}

// Safely get channel mapping with type and size error checking
void getChannelMapping(const mxArray *R, const char *field, int **cmap,
                       int nchans, int nxmits, int nelems) {
  // If user does not provide channel mapping, assume none
  if (mxIsStruct(R)) {
    mxArray *A = mxGetField(R, 0, field);
    if (A == NULL || mxIsEmpty(A)) {
      if (nchans != nelems)
        mexErrMsgTxt(
            "No channel mapping is provided, but the number of acquisition "
            "channels does not match the number of total channels.");
      else {
        *cmap = nullptr;
        return;
      }
    }
  }

  // Otherwise, get channel mapping safely with exist and type checking
  mxArray *A = getRequiredField(R, field, cmap);
  // Check dimensions
  int N = mxGetNumberOfElements(A);
  if (N != nchans * nxmits) {
    mexErrMsgTxt("The channel mapping must be nchans-by-nxmits.");
  }
  int *map = (int *)mxGetData(A);
  for (int i = 0; i < N; i++) {
    if (map[i] >= nelems) {
      mexErrMsgTxt(
          "Channel mapping entry exceeds the actual number of channels.");
    }
  }

  // If still valid, return pointer to channel mapping.
  *cmap = (int *)mxGetData(A);
}

// Get VSXSampleMode enum value
void getVSXSampleMode(const mxArray *R, const char *field, VSXSampleMode *mode,
                      float *spcy) {
  mxArray *A = getRequiredField(R, field);
  if (!mxIsChar(A)) {
    char msg[256];
    sprintf(msg, "%s is the wrong datatype. Expected a string.", field);
    mexErrMsgTxt(msg);
  }
  char *str = mxArrayToString(A);
  if (strcmp(str, "BS50BW") == 0) {
    *mode = VSXSampleMode::BS50BW;
    *spcy = 0.5f;  // Every two samples is the I and Q of one sample
  } else if (strcmp(str, "BS100BW") == 0) {
    *mode = VSXSampleMode::BS100BW;
    *spcy = 1.0f;  // Every two samples is the I and Q of one sample
  } else if (strcmp(str, "NS200BW") == 0) {
    *mode = VSXSampleMode::NS200BW;
    *spcy = 2.0f;  // Every two samples is the I and Q of one sample
  } else if (strcmp(str, "HILBERT") == 0) {
    *mode = VSXSampleMode::HILBERT;
    *spcy = 0.f;
  } else {
    char msg[256];
    sprintf(msg,
            "Unrecognized value for %s. Valid values are 'BS50BW', 'BS100BW', "
            "'NS200BW', or 'HILBERT'.",
            field);
    mexErrMsgTxt(msg);
  }
}

// Get VSXDataOrder enum value
void getVSXDataOrder(const mxArray *R, const char *field, VSXDataOrder *order) {
  mxArray *A = getRequiredField(R, field);
  if (!mxIsChar(A)) {
    char msg[256];
    sprintf(msg, "%s is the wrong datatype. Expected a string.", field);
    mexErrMsgTxt(msg);
  }
  char *str = mxArrayToString(A);
  if (strcmp(str, "PXF") == 0)
    *order = VSXDataOrder::PXF;
  else if (strcmp(str, "PFX") == 0)
    *order = VSXDataOrder::PFX;
  else if (strcmp(str, "XPF") == 0)
    *order = VSXDataOrder::XPF;
  else if (strcmp(str, "FPX") == 0)
    *order = VSXDataOrder::FPX;
  else if (strcmp(str, "XFP") == 0)
    *order = VSXDataOrder::XFP;
  else if (strcmp(str, "FXP") == 0)
    *order = VSXDataOrder::FXP;
  else {
    char msg[256];
    sprintf(msg,
            "Unrecognized value for %s. Valid values are 'PXF', 'PFX', 'XPF', "
            "'FPX', 'XFP', or 'FXP'.",
            field);
    mexErrMsgTxt(msg);
  }
}

// Get the SynthesisMode enum value
void getSynthesisMode(const mxArray *R, const char *field,
                      SynthesisMode *mode) {
  // By default, synthesize the transmit aperture
  *mode = SynthesisMode::SynthTx;
  // If specified, synthesize the receive aperture
  if (mxIsStruct(R)) {
    mxArray *A = mxGetField(R, 0, field);
    if (A != NULL && (int)mxGetScalar(A) == 1) {
      *mode = SynthesisMode::SynthRx;
    }
  }
}

// Get the SynthesisMode enum value
void getInterpMode(const mxArray *R, const char *field, InterpMode *mode) {
  mxArray *A = getRequiredField(R, field);
  if (!mxIsChar(A)) {
    char msg[256];
    sprintf(msg, "%s is the wrong datatype. Expected a string.", field);
    mexErrMsgTxt(msg);
  }
  char *str = mxArrayToString(A);
  if (strcmp(str, "linear") == 0)
    *mode = InterpMode::Linear;
  else if (strcmp(str, "cubic") == 0)
    *mode = InterpMode::Cubic;
  else if (strcmp(str, "lanczos3") == 0)
    *mode = InterpMode::Lanczos3;
  else {
    char msg[256];
    sprintf(msg,
            "Unrecognized value for %s. Valid values are 'linear', 'cubic', "
            "or 'lanczos3'.",
            field);
    mexErrMsgTxt(msg);
  }
}

// Get a C-string
void getString(const mxArray *R, const char *field, char *str) {
  // Load string from mxArray struct
  if (mxIsStruct(R)) {
    mxArray *A = getRequiredField(R, field);
    if (!mxIsChar(A)) {
      char msg[256];
      sprintf(msg, "%s is the wrong datatype. Expected a string.", field);
      mexErrMsgTxt(msg);
    }
    char *Astr = mxArrayToString(A);
    strcpy(str, Astr);
  }
}

void checkFileExists(char *str) {
  // Check that file exists
  if (!std::ifstream(str).good()) {
    char msg[256];
    sprintf(msg, "File %s cannot be accessed.\n", str);
    mexErrMsgTxt(msg);
  }
}

// Copy output of DataProcessor to MATLAB array
// NOTE: The uninterleave is slow because of memory allocation
void copyToMATLABArrayAsync(DataProcessor<float2> *D, mxArray **A,
                            cudaStream_t stream, bool uninterleave) {
  mwSize nrows = D->getOutputDataArray()->getNx();
  mwSize ncols = D->getOutputDataArray()->getNy();
  mwSize nchans = D->getOutputDataArray()->getNc();
  mwSize nframes = D->getOutputDataArray()->getNf();
  mwSize ndim = 4;
  if (uninterleave) {
    const mwSize dims[] = {nrows, ncols, nchans, nframes};
    *A = mxCreateNumericArray(ndim, dims, mxSINGLE_CLASS, mxCOMPLEX);
#if MX_HAS_INTERLEAVED_COMPLEX
    float2 *h_out = (float2 *)mxGetComplexSingles(*A);
    D->getOutputDataArray()->copyFromGPUAsync(h_out, stream);
#else
    float *h_outR = (float *)mxGetData(*A);
    float *h_outI = (float *)mxGetImagData(*A);
    float2 *h_tmp =
        (float2 *)malloc(sizeof(float2) * nrows * ncols * nchans * nframes);
    D->getOutputDataArray()->copyFromGPUAsync(h_tmp, stream);
    for (int f = 0; f < nframes; f++) {
      for (int k = 0; k < nchans; k++) {
        for (int j = 0; j < ncols; j++) {
          for (int i = 0; i < nrows; i++) {
            int idx = i + nrows * (j + ncols * (k + nchans * f));
            h_outR[idx] = h_tmp[idx].x;
            h_outI[idx] = h_tmp[idx].y;
          }
        }
      }
    }
    free(h_tmp);
#endif
  } else {
    const mwSize dims[] = {nrows * 2, ncols, nchans, nframes};
    *A = mxCreateNumericArray(ndim, dims, mxSINGLE_CLASS, mxREAL);
    float2 *h_out = (float2 *)mxGetData(*A);
    D->getOutputDataArray()->copyFromGPUAsync(h_out, stream);
  }
}

// Copy output of DataProcessor to MATLAB array
// NOTE: The uninterleave is slow because of memory allocation
void copyToMATLABArrayAsync(DataProcessor<short2> *D, mxArray **A,
                            cudaStream_t stream, bool uninterleave) {
  mwSize nrows = D->getOutputDataArray()->getNx();
  mwSize ncols = D->getOutputDataArray()->getNy();
  mwSize nchans = D->getOutputDataArray()->getNc();
  mwSize nframes = D->getOutputDataArray()->getNf();
  mwSize ndim = 4;
  if (uninterleave) {
    const mwSize dims[] = {nrows, ncols, nchans, nframes};
    *A = mxCreateNumericArray(ndim, dims, mxINT16_CLASS, mxCOMPLEX);
#if MX_HAS_INTERLEAVED_COMPLEX
    short2 *h_out = (short2 *)mxGetComplexInt16s(*A);
    D->getOutputDataArray()->copyFromGPUAsync(h_out, stream);
#else
    short *h_outR = (short *)mxGetData(*A);
    short *h_outI = (short *)mxGetImagData(*A);
    short2 *h_tmp =
        (short2 *)malloc(sizeof(short2) * nrows * ncols * nchans * nframes);
    D->getOutputDataArray()->copyFromGPUAsync(h_tmp, stream);
    for (int f = 0; f < nframes; f++) {
      for (int k = 0; k < nchans; k++) {
        for (int j = 0; j < ncols; j++) {
          for (int i = 0; i < nrows; i++) {
            int idx = i + nrows * (j + ncols * (k + nchans * f));
            h_outR[idx] = h_tmp[idx].x;
            h_outI[idx] = h_tmp[idx].y;
          }
        }
      }
    }
    free(h_tmp);
#endif
  } else {
    const mwSize dims[] = {nrows * 2, ncols, nchans, nframes};
    *A = mxCreateNumericArray(ndim, dims, mxINT16_CLASS, mxREAL);
    short2 *h_out = (short2 *)mxGetData(*A);
    D->getOutputDataArray()->copyFromGPUAsync(h_out, stream);
  }
}
// Copy output of DataProcessor to MATLAB array
void copyToMATLABArrayAsync(DataProcessor<float> *D, mxArray **A,
                            cudaStream_t stream) {
  mwSize nrows = D->getOutputDataArray()->getNx();
  mwSize ncols = D->getOutputDataArray()->getNy();
  mwSize nchans = D->getOutputDataArray()->getNc();
  mwSize nframes = D->getOutputDataArray()->getNf();
  mwSize ndim = 4;
  const mwSize dims[] = {nrows, ncols, nchans, nframes};
  *A = mxCreateNumericArray(ndim, dims, mxSINGLE_CLASS, mxREAL);
  float *h_out = (float *)mxGetData(*A);
  D->getOutputDataArray()->copyFromGPUAsync(h_out, stream);
}
// Copy output of DataProcessor to MATLAB array
void copyToMATLABArrayAsync(DataProcessor<short> *D, mxArray **A,
                            cudaStream_t stream) {
  mwSize nrows = D->getOutputDataArray()->getNx();
  mwSize ncols = D->getOutputDataArray()->getNy();
  mwSize nchans = D->getOutputDataArray()->getNc();
  mwSize nframes = D->getOutputDataArray()->getNf();
  mwSize ndim = 4;
  const mwSize dims[] = {nrows, ncols, nchans, nframes};
  *A = mxCreateNumericArray(ndim, dims, mxINT16_CLASS, mxREAL);
  short *h_out = (short *)mxGetData(*A);
  D->getOutputDataArray()->copyFromGPUAsync(h_out, stream);
}

}  // namespace rtbf
