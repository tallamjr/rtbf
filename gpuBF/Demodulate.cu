/**
 @file gpuBF/Demodulate.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2021-05-06

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "Demodulate.cuh"

namespace rtbf {
namespace kernels = DemodulateKernels;
template <typename T_in, typename T_out>
Demodulate<T_in, T_out>::Demodulate() {
  this->resetDataProcessor();
  reset();
}
template <typename T_in, typename T_out>
Demodulate<T_in, T_out>::~Demodulate() {
  reset();
}

template <typename T_in, typename T_out>
void Demodulate<T_in, T_out>::reset() {
  if (this->isInit) {
    this->printMsg("Clearing object.");
  }

  // Demodulate class members
  in = nullptr;
  cps = 0.f;

  // Base class members
  this->resetDataProcessor();
  strcpy(this->moniker, "Demodulate");

  // Default mode is in_place
  in_place = true;
}

template <typename T_in, typename T_out>
void Demodulate<T_in, T_out>::initialize(DataArray<T_in> *input,
                                         float samplesPerCycle, bool inPlace,
                                         cudaStream_t cudaStream,
                                         int verbosity) {
  // If previously initialized, reset
  reset();

  // Read inputs
  in = input;
  cps = 1.f / samplesPerCycle;
  in_place = inPlace;

  // Cannot do in place operation if T_in and T_out are different data types
  if (in_place && typeid(T_in) != typeid(T_out)) {
    this->printErr(
        "Input and output data tyep mismatch. Cannot demodulate in-place.",
        __FILE__, __LINE__);
    return;
  }

  // Device information
  this->setDeviceID(input->getDeviceID());
  this->stream = cudaStream;
  this->verb = verbosity;
  this->printMsg("Initializing object.");

  // Make a new DataArray for the output
  if (!in_place) {
    this->out.initialize(in->getDataDim(), this->devID, "Demodulate",
                         this->verb);
  }

  // Initialize arrays
  this->isInit = true;
}

// Alternative initializer using DataProcessor.
template <typename T_in, typename T_out>
void Demodulate<T_in, T_out>::initialize(DataProcessor<T_in> *input,
                                         float samplesPerCycle, bool inPlace,
                                         cudaStream_t cudaStream,
                                         int verbosity) {
  // Pass through to initializer
  initialize(input->getOutputDataArray(), samplesPerCycle, inPlace, cudaStream,
             verbosity);
}

template <typename T_in, typename T_out>
void Demodulate<T_in, T_out>::demodulate() {
  if (!this->isInit) {
    this->printErr("Object not initialized.", __FILE__, __LINE__);
    return;
  }
  // Get the data dimensions
  DataDim idims = in->getDataDim();
  DataDim odims = in_place ? idims : this->out.getDataDim();

  dim3 B(256, 1, 1);
  dim3 G((odims.x - 1) / B.x + 1, (odims.y - 1) / B.y + 1,
         (odims.c - 1) / B.z + 1);

  // Pointers to data
  // NOTE: This is a nasty hack to cast d_in to type T_out*, but this can only
  // occur if in_place, which can only occur if typeid(T_in) == typeid(T_out).
  T_in *d_in = in->getDataPtr();
  T_out *d_out = in_place ? (T_out *)d_in : this->out.getDataPtr();

  // Execute
  CCE(cudaSetDevice(this->devID));
  kernels::demodData<<<G, B, 0, this->stream>>>(d_in, idims, d_out, odims, cps);
}

template <typename T_in, typename T_out>
void Demodulate<T_in, T_out>::setInputDataArray(DataArray<T_in> *input) {
  if (!this->isInit) {
    this->printErr("Object not initialized.", __FILE__, __LINE__);
    return;
  }
  if (!(in->getDataDim() == input->getDataDim())) {
    this->printErr("New input DataArray dimensions do not match.", __FILE__,
                   __LINE__);
    return;
  }
  in = input;
}

template <typename T_in, typename T_out>
DataArray<T_out> *Demodulate<T_in, T_out>::getOutputDataArray() {
  if (in_place) {
    return (DataArray<T_out> *)in;
  } else {
    return &(this->out);
  }
}

template <typename T_in, typename T_out>
__global__ void kernels::demodData(T_in *idata, DataDim idims, T_out *odata,
                                   DataDim odims, float cps) {
  int x = threadIdx.x + blockIdx.x * blockDim.x;
  int y = threadIdx.y + blockIdx.y * blockDim.y;
  int c = threadIdx.z + blockIdx.z * blockDim.z;
  if (x < odims.x && y < odims.y && c < odims.c) {
    // Compute output index
    int iidx = x + idims.p * (y + idims.y * c);
    int oidx = x + odims.p * (y + odims.y * c);
    int iframesz = idims.p * idims.y * idims.c;
    int oframesz = odims.p * odims.y * odims.c;
    for (int frame = 0; frame < odims.f; frame++) {
      // Get complex phasor for demodulation
      float2 demod, if2, of2;
      sincospif(-2.f * x * cps, &demod.y, &demod.x);
      if2 = loadFloat2(idata, iidx);      // Load input as float2
      of2 = complexMultiply(if2, demod);  // Demodulate
      saveData2(odata, oidx, of2);        // Store output
      // Advance pointers by one frame
      idata += iframesz;
      odata += oframesz;
    }
  }
}

// Explicit template specialization instantiation
template class Demodulate<short2, short2>;
template class Demodulate<short2, int2>;
template class Demodulate<short2, float2>;
template class Demodulate<int2, int2>;
template class Demodulate<int2, float2>;
template class Demodulate<float2, float2>;

}  // namespace rtbf
