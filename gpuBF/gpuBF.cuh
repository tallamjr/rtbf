/**
 @file gpuBF/gpuBF.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-03-04

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef GPUBF_CUH_
#define GPUBF_CUH_

#include <cuda.h>
#include <cuda_fp16.h>
#include <cufft.h>
#include <helper_cuda.h>
#include <helper_math.h>
#include <vector_types.h>
#include <cstring>
// #include <string>   // Sigh, MATLAB's libstdc++ is out of date.
#include <iostream>
#include <typeinfo>
#include <vector>
#include "vector_ops.cuh"  // Additional vector operation utilities

// Define macros for excessively long names
#define CCE checkCudaErrors
#define HtoD cudaMemcpyHostToDevice
#define DtoH cudaMemcpyDeviceToHost
const int ERRMSGBUFLEN = 128;

#endif /* GPUBF_CUH_ */
