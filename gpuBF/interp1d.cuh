/**
 @file gpuBF/interp1d.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2021-03-08

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**	This file contains template device code that DataFormatter classes and
 * CUDA kernels can utilize for 1D interpolation.
 */

#ifndef INTERP1D_CUH_
#define INTERP1D_CUH_

#include "gpuBF.cuh"

namespace rtbf {
/// @brief Enum class to select interpolation mode
enum class InterpMode { Linear = 0, Cubic = 1, Lanczos3 = 2 };

namespace interp1d {

/// @brief Device code template for linear interpolation
template <typename T>
__device__ float2 linear(cudaTextureObject_t tex, float x, int idx) {
  return tex2D<T>(tex, x + 0.5f, idx + 0.5f);
}

/// @brief Device code template for cubic Hermite interpolation
template <typename T>
__device__ float2 cubic(cudaTextureObject_t tex, float x, int idx) {
  float xf;
  float u = modff(x, &xf);  // u is the fractional part, xf the integer part
  float2 s0 = tex2D<T>(tex, xf - 1 + 0.5f, idx + 0.5f);
  float2 s1 = tex2D<T>(tex, xf + 0 + 0.5f, idx + 0.5f);
  float2 s2 = tex2D<T>(tex, xf + 1 + 0.5f, idx + 0.5f);
  float2 s3 = tex2D<T>(tex, xf + 2 + 0.5f, idx + 0.5f);
  float a0 = -1 * u * u * u + 2 * u * u - u;
  float a1 = +3 * u * u * u - 5 * u * u + 2;
  float a2 = -3 * u * u * u + 4 * u * u + u;
  float a3 = +1 * u * u * u - 1 * u * u;
  return (s0 * a0 + s1 * a1 + s2 * a2 + s3 * a3) * 0.5f;
}

/// @brief Inline helper code for Lanczos 3-lobe interpolation
inline __host__ __device__ float lanczos_helper(float u, int a) {
  return (u == 0.f) ? 1.f : sinpif(u) * sinpif(u / a) / (PI * PI * u * u);
}

/// @brief Device code template for Lanczos 3-lobe interpolation
template <typename T>
__device__ float2 lanczos3(cudaTextureObject_t tex, float x, int idx) {
  constexpr int a = 2;  // a=2 for 3-lobe Lanczos resampling
  float xf;
  float u = modff(x, &xf);  // u is the fractional part, xf the integer part
  float2 s0 = tex2D<T>(tex, xf - 1 + 0.5f, idx + 0.5f);
  float2 s1 = tex2D<T>(tex, xf + 0 + 0.5f, idx + 0.5f);
  float2 s2 = tex2D<T>(tex, xf + 1 + 0.5f, idx + 0.5f);
  float2 s3 = tex2D<T>(tex, xf + 2 + 0.5f, idx + 0.5f);
  float a0 = lanczos_helper(u + 1, a);
  float a1 = lanczos_helper(u + 0, a);
  float a2 = lanczos_helper(u - 1, a);
  float a3 = lanczos_helper(u - 2, a);
  return s0 * a0 + s1 * a1 + s2 * a2 + s3 * a3;
}

}  // namespace interp1d
}  // namespace rtbf

#endif  // INTERP1D_CUH_