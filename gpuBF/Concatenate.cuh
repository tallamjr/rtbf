/**
 @file gpuBF/Concatenate.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2020-01-21

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CONCATENATE_CUH_
#define CONCATENATE_CUH_

#include "DataProcessor.cuh"

namespace rtbf {

/**	@brief Class to concatenate DataArrays together.

This class accepts N inputs and stacks them together in the channel dimension.
The class can accept up to 8 input DataArrays of type T_in and outputs a single
DataArray of type T_out.
*/
template <typename T_in, typename T_out>
class Concatenate : public DataProcessor<T_out> {
 private:
  static const int MAXINPUTS = 8;
  int ninputs;                     ///< Number of input DataArrays
  DataArray<T_in> *in[MAXINPUTS];  ///< Input DataArray array

 public:
  Concatenate();
  virtual ~Concatenate();

  /// @brief Initialize Concatenate object with a DataArray input
  void initialize(int numInputs, DataArray<T_in> *inputs[],
                  cudaStream_t cudaStream = 0, int verbosity = 1);
  /// @brief Initialize Concatenate object with a DataProcessor input
  void initialize(int numInputs, DataProcessor<T_in> *inputs[],
                  cudaStream_t cudaStream = 0, int verbosity = 1);
  /// @brief Reset object
  void reset();

  /// @brief Concatenate DataArrays together
  void concatenate();
};

namespace ConcatenateKernels {
template <typename T_in, typename T_out>
__global__ void copyChannels(T_in *idata, DataDim idims, T_out *odata,
                             DataDim odims);

}  // namespace ConcatenateKernels
}  // namespace rtbf

#endif /* CONCATENATE_CUH_ */
