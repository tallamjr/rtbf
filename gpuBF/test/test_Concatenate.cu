/**
 @file test/test_Concatenate.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2020-01-21
*/

#include "../Concatenate.cuh"
#include "gtest/gtest.h"

namespace rtbf {
namespace ConcatenateTest {
// Use a test fixture to reuse resource between tests
class ConcatenateTest : public ::testing::Test {
 protected:
  // Re-use the same arrays for all tests
  static const int MAXSIZE = 8;
  static DataArray<float2> *A[MAXSIZE];
  static float2 *data[MAXSIZE];
  static DataDim dim[MAXSIZE];
  // Initialize random data
  static void SetUpTestSuite() {
    for (int n = 0; n < MAXSIZE; n++) {
      // Create a DataArray with dummy data
      dim[n].x = 17;
      dim[n].y = 3;
      dim[n].c = n + 1;
      dim[n].f = 2;
      srand(time(NULL));
      int size = dim[n].x * dim[n].y * dim[n].c * dim[n].f;
      data[n] = (float2 *)malloc(sizeof(float2) * size);
      for (int i = 0; i < size; i++) {
        data[n][i].x = (float)(rand() % 16) * 1.13f;
        data[n][i].y = (float)(rand() % 16) * .998f;
      }
      // Copy data to GPU
      A[n] = new DataArray<float2>;
      A[n]->initialize(dim[n]);
      A[n]->copyToGPU(data[n]);
    }
  }
  // Free memory
  static void TearDownTestSuite() {
    for (int n = 0; n < 8; n++) {
      A[n]->reset();
      free(data[n]);
    }
  }
};

DataArray<float2> *ConcatenateTest::A[];
float2 *ConcatenateTest::data[];
DataDim ConcatenateTest::dim[];

double compute_sqerr(float2 *a, float2 *b, int size) {
  double sqerr = 0.0;
  for (int i = 0; i < size; i++) {
    double errR = (a[i].x - b[i].x);
    double errI = (a[i].y - b[i].y);
    sqerr += errR * errR + errI * errI;
  }
  return sqerr;
}

void concatenate_CPU(int ninputs, float2 *in[], DataDim d_in[], float2 *out) {
  int nx = d_in[0].x;
  int ny = d_in[0].y;
  int nf = d_in[0].f;
  int nchout = 0;
  for (int n = 0; n < ninputs; n++) nchout += d_in[n].c;

  memset(out, 0, sizeof(float2) * nx * ny * nchout * nf);
  int co = 0;
  for (int n = 0; n < ninputs; n++) {
    for (int ci = 0; ci < d_in[n].c; ci++) {
      for (int f = 0; f < nf; f++) {
        for (int y = 0; y < ny; y++) {
          for (int x = 0; x < nx; x++) {
            int oidx = x + nx * (y + ny * (co + nchout * f));
            int iidx = x + nx * (y + ny * (ci + d_in[n].c * f));
            out[oidx] = in[n][iidx];
          }
        }
      }
      co++;  // Advance channel of output
    }
  }
}

// Test concatenating channels
TEST_F(ConcatenateTest, TwoInputs) {
  // Create a Concatenate object
  int ninputs = 2;
  Concatenate<float2, float2> C;
  C.initialize(ninputs, A, 0, 0);

  // Concatenate on CPU
  int nchout = 0;
  for (int n = 0; n < ninputs; n++) nchout += dim[n].c;
  int size = dim[0].x * dim[0].y * dim[0].f * nchout;
  float2 *conCPU = (float2 *)malloc(sizeof(float2) * size);
  float2 *conGPU = (float2 *)malloc(sizeof(float2) * size);
  concatenate_CPU(ninputs, data, dim, conCPU);
  // Sum channels on GPU
  C.concatenate();
  C.getOutputDataArray()->copyFromGPU(conGPU);

  // Test for accuracy
  double sqerr = compute_sqerr(conCPU, conGPU, size);
  EXPECT_EQ(sqerr, 0.0);

  free(conCPU);
  free(conGPU);
  C.reset();
}

// Test concatenating channels
TEST_F(ConcatenateTest, EightInputs) {
  // Create a Concatenate object
  int ninputs = 8;
  Concatenate<float2, float2> C;
  C.initialize(ninputs, A, 0, 0);

  // Concatenate on CPU
  int nchout = 0;
  for (int n = 0; n < ninputs; n++) nchout += dim[n].c;
  int size = dim[0].x * dim[0].y * dim[0].f * nchout;
  float2 *conCPU = (float2 *)malloc(sizeof(float2) * size);
  float2 *conGPU = (float2 *)malloc(sizeof(float2) * size);
  concatenate_CPU(ninputs, data, dim, conCPU);
  // Sum channels on GPU
  C.concatenate();
  C.getOutputDataArray()->copyFromGPU(conGPU);

  // Test for accuracy
  double sqerr = compute_sqerr(conCPU, conGPU, size);
  EXPECT_EQ(sqerr, 0.0);

  free(conCPU);
  free(conGPU);
  C.reset();
}

// Test concatenating channels
TEST_F(ConcatenateTest, NestedInputs) {
  // Create a Concatenate object
  int ninputs = 2;
  Concatenate<float2, float2> B, C, D;
  DataProcessor<float2> *BC[] = {&B, &C};
  B.initialize(ninputs, A, 0, 0);
  C.initialize(ninputs, &A[ninputs], 0, 0);
  D.initialize(2, BC, 0, 0);

  // Concatenate on CPU
  int nchout = 0;
  for (int n = 0; n < ninputs * 2; n++) nchout += dim[n].c;
  int size = dim[0].x * dim[0].y * dim[0].f * nchout;
  float2 *conCPU = (float2 *)malloc(sizeof(float2) * size);
  float2 *conGPU = (float2 *)malloc(sizeof(float2) * size);
  concatenate_CPU(ninputs * 2, data, dim, conCPU);
  // Sum channels on GPU
  B.concatenate();
  C.concatenate();
  D.concatenate();
  D.getOutputDataArray()->copyFromGPU(conGPU);

  // Test for accuracy
  double sqerr = compute_sqerr(conCPU, conGPU, size);
  EXPECT_EQ(sqerr, 0.0);

  free(conGPU);
  free(conCPU);
  D.reset();
  C.reset();
  B.reset();
}

}  // namespace ConcatenateTest
}  // namespace rtbf