/**
 @file test/test_ChannelCoherence.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-08-17
*/

#include "../ChannelCoherence.cuh"
#include "gtest/gtest.h"

namespace rtbf {
namespace ChannelCoherenceTest {
// Use a test fixture to reuse resource between tests
class ChannelCoherenceTest : public ::testing::Test {
 protected:
  // Re-use the same arrays for all tests
  static DataArray<float2> A;
  static float2 *data;
  static DataDim dim;
  static float *Cxy, *Cxx, *Cyy;
  // Initialize random data
  static void SetUpTestSuite() {
    // Create a DataArray with dummy data
    dim.x = 17;
    dim.y = 9;
    dim.c = 16;
    dim.f = 2;
    srand(time(NULL));
    int size = dim.x * dim.y * dim.c * dim.f;
    data = (float2 *)malloc(sizeof(float2) * size);
    for (int i = 0; i < size; i++) {
      data[i].x = (float)(rand() % 16) * 1.13f;
      data[i].y = (float)(rand() % 16) * .998f;
    }
    // Copy data to GPU
    A.initialize(dim);
    A.copyToGPU(data);
  }
  // Free memory
  static void TearDownTestSuite() {
    A.reset();
    free(data);
  }
};

DataArray<float2> ChannelCoherenceTest::A;
float2 *ChannelCoherenceTest::data;
DataDim ChannelCoherenceTest::dim;

double compute_MSE(float *a, float *b, int size) {
  double mse = 0.0;
  for (int i = 0; i < size; i++) {
    mse += (a[i] - b[i]) * (a[i] - b[i]);
  }
  return mse;
}
double compute_MAE(float *a, float *b, int size) {
  double mae = 0.0;
  for (int i = 0; i < size; i++) {
    mae += abs(a[i] - b[i]);
  }
  return mae;
}

float cmultR(float2 a, float2 b) { return a.x * b.x + a.y * b.y; }

void corrcoef_CPU(float2 *in, DataDim d_in, float *out, int nlags, int hkA,
                  int hkL) {
  memset(out, 0, sizeof(float) * d_in.x * d_in.y * d_in.f * nlags);
  for (int m = 0; m < nlags; m++) {
    int lag = m + 1;
    for (int f = 0; f < d_in.f; f++) {
      for (int j = 0; j < d_in.y; j++) {
        for (int i = 0; i < d_in.x; i++) {
          // Initialize running sums
          float Sxy = 0.f;
          float Sxx = 0.f;
          float Syy = 0.f;
          // Loop over kernel (channels, lateral, axial)
          for (int c = 0; c < d_in.c - lag; c++) {
            int offset0 = d_in.x * d_in.y * (c + d_in.c * f);
            int offsetm = d_in.x * d_in.y * (c + lag + d_in.c * f);
            for (int jj = -hkL; jj <= hkL; jj++) {
              for (int ii = -hkA; ii <= hkA; ii++) {
                int iii = ii + i;
                int jjj = jj + j;
                if (iii >= 0 && iii < d_in.x && jjj >= 0 && jjj < d_in.y) {
                  float2 x = in[iii + d_in.x * jjj + offset0];
                  float2 y = in[iii + d_in.x * jjj + offsetm];
                  Sxy += cmultR(x, y);
                  Sxx += cmultR(x, x);
                  Syy += cmultR(y, y);
                }
              }
            }
          }
          // Compute correlation coefficient
          int idxo = i + d_in.x * (j + d_in.y * (m + nlags * f));
          out[idxo] = Sxy / sqrt(Sxx) / sqrt(Syy);
        }
      }
    }
  }
}

// Simple test of two scalars that are proportional to one another
TEST_F(ChannelCoherenceTest, Scalar) {
  int nlags = 1;
  int kernszA = 1;
  int kernszL = 1;

  // Create dummy data of a scalar in 2 channels
  float2 d[] = {make_float2(1.f, 0.5f), make_float2(-1.4f, -0.7f)};
  // Copy dummy data to a DataArray
  DataDim ddim;
  ddim.x = 1;
  ddim.y = 1;
  ddim.c = 2;
  ddim.f = 1;
  DataArray<float2> D;
  D.initialize(ddim);
  D.copyToGPU(d);

  // Create a ChannelCoherence object
  ChannelCoherence<float2, float> S;
  S.initialize(&D, nlags, 0, 0);

  // Correlate channels on CPU
  float resCPU, resGPU;
  int hkA = int(kernszA / 2);
  int hkL = int(kernszL / 2);
  corrcoef_CPU(d, ddim, &resCPU, nlags, hkA, hkL);
  // correlate channels on GPU
  S.computeCoherence(kernszA, kernszL);
  S.getOutputDataArray()->copyFromGPU(&resGPU);

  // Test for accuracy
  EXPECT_NEAR(resCPU, -1.0, 0.0001);
  EXPECT_NEAR(resGPU, -1.0, 0.0001);

  S.reset();
  D.reset();
}

// Test the CPU and GPU versions versus a known ground truth
TEST_F(ChannelCoherenceTest, Kern0D) {
  // Create dummy data of a scalar in 128 channels
  const int nchans = 128;
  float2 d[nchans];
  for (int i = 0; i < nchans; i++) {
    d[i] = make_float2((i + 1) * 13.7f, 0.0f);
  }
  // Copy dummy data to a DataArray
  DataDim ddim;
  ddim.x = 1;
  ddim.y = 1;
  ddim.c = nchans;
  ddim.f = 1;
  DataArray<float2> D;
  D.initialize(ddim);
  D.copyToGPU(d);
  const int nlags = nchans - 1;
  int kernszA = 1;
  int kernszL = 1;

  // Create a ChannelCoherence object
  ChannelCoherence<float2, float> S;
  S.initialize(&D, nlags, 0, 0);

  // Correlate channels on CPU
  float resCPU[nlags], resGPU[nlags];
  int hkA = int(kernszA / 2);
  int hkL = int(kernszL / 2);
  corrcoef_CPU(d, ddim, resCPU, nlags, hkA, hkL);
  // correlate channels on GPU
  S.computeCoherence(kernszA, kernszL);
  S.getOutputDataArray()->copyFromGPU(resGPU);

  float truth[] = {
      0.99998, 0.99991, 0.99980, 0.99965, 0.99946, 0.99923, 0.99896, 0.99866,
      0.99832, 0.99794, 0.99753, 0.99708, 0.99660, 0.99609, 0.99555, 0.99498,
      0.99438, 0.99375, 0.99309, 0.99241, 0.99170, 0.99097, 0.99022, 0.98944,
      0.98863, 0.98781, 0.98697, 0.98610, 0.98522, 0.98432, 0.98340, 0.98246,
      0.98151, 0.98054, 0.97955, 0.97855, 0.97754, 0.97651, 0.97547, 0.97442,
      0.97336, 0.97228, 0.97119, 0.97010, 0.96899, 0.96788, 0.96675, 0.96562,
      0.96448, 0.96334, 0.96219, 0.96103, 0.95986, 0.95869, 0.95752, 0.95634,
      0.95515, 0.95396, 0.95277, 0.95158, 0.95038, 0.94918, 0.94798, 0.94678,
      0.94558, 0.94437, 0.94317, 0.94196, 0.94076, 0.93955, 0.93835, 0.93715,
      0.93595, 0.93475, 0.93355, 0.93236, 0.93116, 0.92998, 0.92879, 0.92761,
      0.92644, 0.92527, 0.92410, 0.92294, 0.92179, 0.92064, 0.91950, 0.91837,
      0.91725, 0.91614, 0.91503, 0.91394, 0.91285, 0.91178, 0.91072, 0.90968,
      0.90865, 0.90764, 0.90665, 0.90567, 0.90472, 0.90379, 0.90289, 0.90201,
      0.90117, 0.90037, 0.89960, 0.89888, 0.89822, 0.89761, 0.89708, 0.89663,
      0.89628, 0.89604, 0.89595, 0.89604, 0.89634, 0.89693, 0.89788, 0.89933,
      0.90147, 0.90460, 0.90926, 0.91644, 0.92823, 0.94992, 1.00000};
  // Test for accuracy
  for (int i = 0; i < nlags; i++) {
    EXPECT_NEAR(resCPU[i], truth[i], 0.0001);
    EXPECT_NEAR(resGPU[i], truth[i], 0.0001);
  }
  // Clean up
  S.reset();
  D.reset();
}

// Test an axial kernel using the test fixture
TEST_F(ChannelCoherenceTest, Kern1D) {
  int nlags = 1;
  int kernszA = 9;
  int kernszL = 1;
  // Create a ChannelCoherence object
  ChannelCoherence<float2, float> S;
  S.initialize(&A, nlags, 0, 0);
  // Correlate channels on CPU
  int size = dim.x * dim.y * dim.f * nlags;
  float *resCPU = (float *)malloc(sizeof(float) * size);
  float *resGPU = (float *)malloc(sizeof(float) * size);
  int hkA = int(kernszA / 2);
  int hkL = int(kernszL / 2);
  corrcoef_CPU(data, dim, resCPU, nlags, hkA, hkL);
  // correlate channels on GPU
  S.computeCoherence(kernszA, kernszL);
  S.getOutputDataArray()->copyFromGPU(resGPU);

  // Test for accuracy
  double mae = compute_MAE(resCPU, resGPU, size);
  double mse = compute_MSE(resCPU, resGPU, size);
  EXPECT_NEAR(mae, 0.0, 0.0001);
  EXPECT_NEAR(mse, 0.0, 0.0001);

  free(resCPU);
  free(resGPU);
  S.reset();
}

// Test a 2D kernel using the test fixture
TEST_F(ChannelCoherenceTest, Kern2D) {
  int nlags = 1;
  int kernszA = 9;
  int kernszL = 5;
  // Create a ChannelCoherence object
  ChannelCoherence<float2, float> S;
  S.initialize(&A, nlags, 0, 0);
  // Correlate channels on CPU
  int size = dim.x * dim.y * dim.f * nlags;
  float *resCPU = (float *)malloc(sizeof(float) * size);
  float *resGPU = (float *)malloc(sizeof(float) * size);
  int hkA = int(kernszA / 2);
  int hkL = int(kernszL / 2);
  corrcoef_CPU(data, dim, resCPU, nlags, hkA, hkL);
  // correlate channels on GPU
  S.computeCoherence(kernszA, kernszL);
  S.getOutputDataArray()->copyFromGPU(resGPU);

  // Test for accuracy
  double mae = compute_MAE(resCPU, resGPU, size);
  double mse = compute_MSE(resCPU, resGPU, size);
  EXPECT_NEAR(mae, 0.0, 0.0001);
  EXPECT_NEAR(mse, 0.0, 0.0001);

  free(resCPU);
  free(resGPU);
  S.reset();
}

}  // namespace ChannelCoherenceTest
}  // namespace rtbf