# Add unit tests
add_executable(unittest_gpuBF
    test_gpuBF.cu
    test_DataArray.cu
    test_Bmode.cu
    test_ChannelSum.cu
    test_Concatenate.cu
    test_ChannelCoherence.cu
)

target_link_libraries(unittest_gpuBF
    gtest_main
    gpuBF
)

set_target_properties(unittest_gpuBF PROPERTIES CUDA_RESOLVE_DEVICE_SYMBOLS ON)
add_test(NAME gpuBF_tests COMMAND unittest_gpuBF)