/**
 @file gpuBF/ChannelCoherence.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2020-02-06

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "ChannelCoherence.cuh"

namespace rtbf {
namespace kernels = ChannelCoherenceKernels;
template <typename T_in, typename T_out>
ChannelCoherence<T_in, T_out>::ChannelCoherence() {
  this->resetDataProcessor();
  reset();
}
template <typename T_in, typename T_out>
ChannelCoherence<T_in, T_out>::~ChannelCoherence() {
  reset();
}

template <typename T_in, typename T_out>
void ChannelCoherence<T_in, T_out>::reset() {
  if (this->isInit) {
    this->printMsg("Clearing object.");
  }

  // ChannelCoherence class members
  in = nullptr;
  nlags = 0;

  // Base class members
  this->resetDataProcessor();
  strcpy(this->moniker, "ChannelCoherence");
}

template <typename T_in, typename T_out>
void ChannelCoherence<T_in, T_out>::initialize(DataArray<T_in> *input,
                                               int numLags,
                                               cudaStream_t cudaStream,
                                               int verbosity) {
  // If previously initialized, reset
  reset();

  // Read inputs
  in = input;
  nlags = numLags;

  // Device information
  this->setDeviceID(input->getDeviceID());
  this->stream = cudaStream;
  this->verb = verbosity;
  this->printMsg("Initializing object.");

  // Make a new DataArray for the output
  this->out.initialize(in->getNx(), in->getNy(), nlags, in->getNf(),
                       this->devID, "Channel Coherence", this->verb);

  // Initialize arrays
  this->isInit = true;
}

// Alternative initializer using DataProcessor.
template <typename T_in, typename T_out>
void ChannelCoherence<T_in, T_out>::initialize(DataProcessor<T_in> *input,
                                               int numLags,
                                               cudaStream_t cudaStream,
                                               int verbosity) {
  // Pass through to initializer
  initialize(input->getOutputDataArray(), numLags, cudaStream, verbosity);
}

template <typename T_in, typename T_out>
void ChannelCoherence<T_in, T_out>::computeCoherence(int axkern, int latkern) {
  if (!this->isInit) {
    this->printErr("Object not initialized.", __FILE__, __LINE__);
    return;
  }
  // Compute half kernel size (use floor division)
  int hkA = int(axkern / 2);
  int hkL = int(latkern / 2);

  // Get the data dimensions and set up grid accordingly
  DataDim idims = in->getDataDim();
  DataDim odims = this->out.getDataDim();
  dim3 B(256, 1, 1);
  dim3 G((odims.x - 1) / B.x + 1, (odims.y - 1) / B.y + 1,
         (odims.c - 1) / B.z + 1);
  cudaStream_t strm = this->stream;

  // Loop through frames
  CCE(cudaSetDevice(this->devID));
  for (int frame = 0; frame < in->getNf(); frame++) {
    // Get pointers to the data
    T_in *dat = in->getFramePtr(frame);
    T_out *ccs = this->out.getFramePtr(frame);
    // Choose kernel based on configuration and kernel size
    if (hkA == 0 && hkL == 0) {  // If no kernel
      // Single-sample version (skips bounds checking, etc.)
      kernels::corr0D<<<G, B, 0, strm>>>(dat, idims, ccs, odims);
    } else if (hkL == 0) {  // If 1D kernel
      // Axial kernel version (skips bounds checking on lateral extent)
      kernels::corr1D<<<G, B, 0, strm>>>(dat, idims, ccs, odims, hkA);
    } else {  // If 2D kernel
      // 2D kernel version (slowest)
      kernels::corr2D<<<G, B, 0, strm>>>(dat, idims, ccs, odims, hkA, hkL);
    }
  }
}

template <typename T_in, typename T_out>
void ChannelCoherence<T_in, T_out>::setInputDataArray(DataArray<T_in> *input) {
  if (!this->isInit) {
    this->printErr("Object not initialized.", __FILE__, __LINE__);
    return;
  }
  if (!(in->getDataDim() == input->getDataDim())) {
    this->printErr("New input DataArray dimensions do not match.", __FILE__,
                   __LINE__);
    return;
  }
  in = input;
}

__device__ float kernels::cmultR(float2 a, float2 b) {
  return a.x * b.x + a.y * b.y;
}

// Compute a single-sample correlation coefficient
template <typename T_in, typename T_out>
__global__ void kernels::corr0D(T_in *dat, DataDim idims, T_out *ccs,
                                DataDim odims) {
  int row = threadIdx.x + blockIdx.x * blockDim.x;
  int col = threadIdx.y + blockIdx.y * blockDim.y;
  int lagIdx = threadIdx.z + blockIdx.z * blockDim.z;
  if (row < odims.x && col < odims.y && lagIdx < odims.c) {
    // Compute input, output index offsets
    int iidx = row + idims.p * col;
    int oidx = row + odims.p * (col + odims.y * lagIdx);
    // Actual lag is lagIdx + 1
    int lag = lagIdx + 1;
    // For each channel, compute the cross-correlation and autocorrelations
    // Compute only the real components, i.e., assume C(-lag) == C(lag)
    float xy = 0.f;
    float xx = 0.f;
    float yy = 0.f;
    for (int ch = 0; ch < idims.c - lag; ch++) {
      // Use loadFloat2 to load float2 instead of T_in
      float2 x = loadFloat2(dat, iidx + idims.p * idims.y * ch);
      float2 y = loadFloat2(dat, iidx + idims.p * idims.y * (ch + lag));
      // Accumulate cross-correlation and autocorrelations
      xy += kernels::cmultR(x, y);
      xx += kernels::cmultR(x, x);
      yy += kernels::cmultR(y, y);
    }
    // Compute correlation coefficient
    ccs[oidx] = xy * rsqrtf(xx) * rsqrtf(yy);
  }
}

// Compute a correlation coefficient with an axial kernel
template <typename T_in, typename T_out>
__global__ void kernels::corr1D(T_in *dat, DataDim idims, T_out *ccs,
                                DataDim odims, int halfkA) {
  int row = threadIdx.x + blockIdx.x * blockDim.x;
  int col = threadIdx.y + blockIdx.y * blockDim.y;
  int lagIdx = threadIdx.z + blockIdx.z * blockDim.z;
  if (row < odims.x && col < odims.y && lagIdx < odims.c) {
    // Advance input to current column and compute output index
    dat += idims.p * col;
    int oidx = row + odims.p * (col + odims.y * lagIdx);
    // Actual lag is lagIdx + 1
    int lag = lagIdx + 1;
    // Compute kernel edges
    int row0 = row - halfkA;
    int row1 = row + halfkA;
    // Clip at data edges
    row0 = row0 < 0 ? 0 : row0;
    row1 = row1 >= idims.x ? idims.x - 1 : row1;
    // Initialize running sums
    float xy = 0.f;  // Cross-correlation of x and y
    float xx = 0.f;  // Autocorrelation of x
    float yy = 0.f;  // Autocorrelation of y
    // Outer loop over channels
    for (int ch = 0; ch < idims.c - lag; ch++) {
      // Inner loop over axial samples (rows) to take advantage of cache
      for (int rk = row0; rk <= row1; rk++) {
        // Use loadFloat2 to load float2 instead of T_in
        float2 x = loadFloat2(dat, rk + idims.p * idims.y * ch);
        float2 y = loadFloat2(dat, rk + idims.p * idims.y * (ch + lag));
        // Accumulate cross-correlation and autocorrelations
        // Compute only the real components, i.e., assume C(-lag) == C(lag)
        xy += kernels::cmultR(x, y);
        xx += kernels::cmultR(x, x);
        yy += kernels::cmultR(y, y);
      }
    }
    // Compute correlation coefficient
    ccs[oidx] = xy * rsqrtf(xx) * rsqrtf(yy);
  }
}

// Compute a correlation coefficient with a 2D kernel
template <typename T_in, typename T_out>
__global__ void kernels::corr2D(T_in *dat, DataDim idims, T_out *ccs,
                                DataDim odims, int halfkA, int halfkL) {
  int row = threadIdx.x + blockIdx.x * blockDim.x;
  int col = threadIdx.y + blockIdx.y * blockDim.y;
  int lagIdx = threadIdx.z + blockIdx.z * blockDim.z;
  if (row < odims.x && col < odims.y && lagIdx < odims.c) {
    // Compute output index
    int oidx = row + odims.p * (col + odims.y * lagIdx);
    // Actual lag is lagIdx + 1
    int lag = lagIdx + 1;
    // Compute kernel edges
    int row0 = row - halfkA;
    int row1 = row + halfkA;
    int col0 = col - halfkL;
    int col1 = col + halfkL;
    // Clip at data edges
    row0 = row0 < 0 ? 0 : row0;
    row1 = row1 >= idims.x ? idims.x - 1 : row1;
    col0 = col0 < 0 ? 0 : col0;
    col1 = col1 >= idims.y ? idims.y - 1 : col1;
    // Initialize running sums
    float xy = 0.f;  // Cross-correlation of x and y
    float xx = 0.f;  // Autocorrelation of x
    float yy = 0.f;  // Autocorrelation of y
    // Outer loop over channels
    for (int ch = 0; ch < idims.c - lag; ch++) {
      // Middle loop over lateral samples (columns)
      for (int c = col0; c <= col1; c++) {
        // Inner loop over axial samples (rows) to take advantage of cache
        for (int r = row0; r <= row1; r++) {
          // Use loadFloat2 to load float2 instead of T_in
          float2 x = loadFloat2(dat, r + idims.p * (c + idims.y * ch));
          float2 y = loadFloat2(dat, r + idims.p * (c + idims.y * (ch + lag)));
          // Accumulate cross-correlation and autocorrelations
          // Compute only the real components, i.e., assume C(-lag) == C(lag)
          xy += kernels::cmultR(x, y);
          xx += kernels::cmultR(x, x);
          yy += kernels::cmultR(y, y);
        }
      }
    }
    // Compute correlation coefficient
    ccs[oidx] = xy * rsqrtf(xx) * rsqrtf(yy);
  }
}

// Explicit template specialization instantiation
template class ChannelCoherence<short2, float>;
template class ChannelCoherence<int2, float>;
template class ChannelCoherence<float2, float>;

}  // namespace rtbf
