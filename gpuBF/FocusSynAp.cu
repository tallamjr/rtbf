/**
 @file gpuBF/FocusSynAp.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-03-04

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "FocusSynAp.cuh"

namespace rtbf {
namespace kernels = FocusSynApKernels;
using IM = InterpMode;
using SM = SynthesisMode;
template <typename T_in, typename T_out>
FocusSynAp<T_in, T_out>::FocusSynAp() {
  this->resetDataProcessor();
  reset();
}
template <typename T_in, typename T_out>
FocusSynAp<T_in, T_out>::~FocusSynAp() {
  reset();
}

template <typename T_in, typename T_out>
void FocusSynAp<T_in, T_out>::reset() {
  CCE(cudaSetDevice(this->devID));
  if (this->isInit) {
    this->printMsg("Clearing object.");
    CCE(cudaDestroyTextureObject(tex));
  }
  // FocusSynAp members
  memset(&texDesc, 0, sizeof(texDesc));
  memset(&resDesc, 0, sizeof(resDesc));
  smode = SM::SynthRx;
  imode = IM::Cubic;
  raw = nullptr;
  nrows = 0;
  ncols = 0;
  nxmits = 0;
  nchans = 0;
  spc = 0.f;
  delTx.reset();
  delRx.reset();
  apoTx.reset();
  apoRx.reset();

  // Base class members
  this->resetDataProcessor();
  strcpy(this->moniker, "FocusSynAp");
}

// Initializer
template <typename T_in, typename T_out>
void FocusSynAp<T_in, T_out>::initialize(
    DataArray<T_in> *input, int nOutputRows, int nOutputCols,
    float outputSampsPerWL, float *h_delTx, float *h_delRx, float *h_apoTx,
    float *h_apoRx, float samplesPerCycle, SM synthMode, IM intrpMode,
    cudaStream_t cudaStream, int verbosity) {
  // If previously initialized, reset
  reset();

  // Set DataProcessor members
  this->setDeviceID(input->getDeviceID());
  this->stream = cudaStream;
  this->verb = verbosity;
  this->printMsg("Initializing object.");

  // Parse inputs
  raw = input;
  nrows = nOutputRows;
  ncols = nOutputCols;
  smode = synthMode;
  imode = intrpMode;
  nxmits = raw->getNy();
  nchans = raw->getNc();
  outwlps = 1.f / outputSampsPerWL;
  spc = samplesPerCycle;  // For baseband data
  if (spc > 0.f) this->printMsg("Using baseband input data.", 2);

  // If Tx apodization is provided, Tx delay must be provided as well.
  if (h_apoTx != nullptr && h_delTx == nullptr)
    this->printErr("Specified apodization without delay table.", __FILE__,
                   __LINE__);
  // If one apodization table is provided, the other must be provided as well
  if ((h_apoTx == nullptr) != (h_apoRx == nullptr))
    this->printErr("Only one apodization provided.", __FILE__, __LINE__);

  // Initialize delay and apodization arrays and output
  CCE(cudaSetDevice(this->devID));
  if (smode == SM::SynthTx)
    this->out.initialize(nrows, ncols, nchans, raw->getNf(), this->devID,
                         "FocusSynAp", this->verb);
  else if (smode == SM::SynthRx)
    this->out.initialize(nrows, ncols, nxmits, raw->getNf(), this->devID,
                         "FocusSynAp", this->verb);

  // Initialize transmit and receive delay tables
  delTx.initialize(nrows, ncols, nxmits, 1, this->devID, "FocusSynAp: Tx Delay",
                   this->verb);
  delRx.initialize(nrows, ncols, nchans, 1, this->devID, "FocusSynAp: Rx Delay",
                   this->verb);
  delTx.copyToGPU(h_delTx);
  delRx.copyToGPU(h_delRx);

  // If provided, initialize the transmit and receive apodization tables
  if (h_apoTx != nullptr && h_apoRx != nullptr) {
    apoTx.initialize(nrows, ncols, nxmits, 1, this->devID,
                     "FocusSynAp: Tx Apod", this->verb);
    apoRx.initialize(nrows, ncols, nchans, 1, this->devID,
                     "FocusSynAp: Rx Apod", this->verb);
    apoTx.copyToGPU(h_apoTx);
    apoRx.copyToGPU(h_apoRx);
  }

  // Initialize texture object for input
  initTextureObject(raw->getDataPtr());

  // Mark as initialized
  this->isInit = true;
}

template <typename T_in, typename T_out>
void FocusSynAp<T_in, T_out>::initialize(
    DataProcessor<T_in> *input, int nOutputRows, int nOutputCols,
    float outputSampsPerWL, float *h_delTx, float *h_delRx, float *h_apoTx,
    float *h_apoRx, float samplesPerCycle, SM synthMode, IM intrpMode,
    cudaStream_t cudaStream, int verbosity) {
  // Pass through to initializer
  initialize(input->getOutputDataArray(), nOutputRows, nOutputCols,
             outputSampsPerWL, h_delTx, h_delRx, h_apoTx, h_apoRx,
             samplesPerCycle, synthMode, intrpMode, cudaStream, verbosity);
}

template <typename T_in, typename T_out>
void FocusSynAp<T_in, T_out>::initTextureObject(T_in *d_ptr) {
  // Set up textures to use the built-in bilinear interpolation hardware
  // Use texture objects (CC >= 3.0)
  // Texture description
  texDesc.addressMode[0] = cudaAddressModeBorder;
  texDesc.addressMode[1] = cudaAddressModeBorder;
  texDesc.filterMode = cudaFilterModeLinear;
  texDesc.normalizedCoords = 0;
  texDesc.readMode = cudaReadModeElementType;
  // Only if input is 16-bit integer and output is float2
  if (typeid(T_in) == typeid(short2) && typeid(T_out) == typeid(float2))
    texDesc.readMode = cudaReadModeNormalizedFloat;
  // Resource description
  DataDim d = raw->getDataDim();
  resDesc.resType = cudaResourceTypePitch2D;
  resDesc.res.pitch2D.width = d.x;
  resDesc.res.pitch2D.height = d.y * d.c;
  resDesc.res.pitch2D.desc = cudaCreateChannelDesc<T_in>();
  resDesc.res.pitch2D.devPtr = d_ptr;
  resDesc.res.pitch2D.pitchInBytes = raw->getPitchInBytes();
  // Create texture object
  CCE(cudaSetDevice(this->devID));
  CCE(cudaCreateTextureObject(&tex, &resDesc, &texDesc, NULL));
}

template <typename T_in, typename T_out>
void FocusSynAp<T_in, T_out>::setInputDataArray(DataArray<T_in> *input) {
  if (!this->isInit) {
    this->printErr("Object not initialized.", __FILE__, __LINE__);
    return;
  }
  if (!(raw->getDataDim() == input->getDataDim())) {
    this->printErr("New input DataArray dimensions do not match.", __FILE__,
                   __LINE__);
    return;
  }
  CCE(cudaSetDevice(this->devID));
  CCE(cudaDestroyTextureObject(tex));
  raw = input;
  initTextureObject(raw->getDataPtr());
}

template <typename T_in, typename T_out>
void FocusSynAp<T_in, T_out>::addGlobalDelay(float globalDelay) {
  if (!this->isInit) {
    this->printErr("Object not initialized.", __FILE__, __LINE__);
    return;
  }
  // Get the delay table dimensions
  DataDim dims = delRx.getDataDim();
  CCE(cudaSetDevice(this->devID));
  dim3 B(256, 1, 1);
  dim3 G((dims.x - 1) / B.x + 1, (dims.y - 1) / B.y + 1,
         (dims.c - 1) / B.z + 1);
  kernels::addGlobalDelay<<<G, B, 0, this->stream>>>(dims, delRx.getDataPtr(),
                                                     globalDelay);
}

template <typename T_in, typename T_out>
void FocusSynAp<T_in, T_out>::focus() {
  if (!this->isInit) {
    this->printErr("Object not initialized.", __FILE__, __LINE__);
    return;
  }
  // Set CUDA device
  CCE(cudaSetDevice(this->devID));

  // Get CUDA kernel inputs
  float *d_delTx = delTx.getDataPtr();
  float *d_delRx = delRx.getDataPtr();
  float *d_apoTx = apoTx.getDataPtr();  // Fetches nullptr if not initialized
  float *d_apoRx = apoRx.getDataPtr();  // Fetches nullptr if not initialized

  // Determine which CUDA kernel to call
  bool isRF = spc == 0.f;
  bool isApo = d_apoTx != nullptr && d_apoRx != nullptr;
  focusArgs args;
  // If synthesizing the transmit aperture and obtaining the receive
  // aperture, call focus_synth with delSynth=d_delTx and delFocus = d_delRx
  // and vice versa for synthesizing the receive aperture and obtaining the
  // focused transmit data.
  if (smode == SM::SynthTx) {
    args.delSynth = d_delTx;    // Sum the transmits
    args.delFocus = d_delRx;    // Preserve the receive channels
    args.apoSynth = d_apoTx;    // Sum the transmits
    args.apoFocus = d_apoRx;    // Preserve the receive channels
    args.nSynth = nxmits;       // nxmits samples to sum
    args.nFocus = nchans;       // nchans samples to keep
    args.strideSynth = 1;       // Each xmit is separated by 1
    args.strideFocus = nxmits;  // Each chan is separated by nxmits
  } else if (smode == SM::SynthRx) {
    args.delSynth = d_delRx;    // Sum the receive channels
    args.delFocus = d_delTx;    // Preserve the transmits
    args.apoSynth = d_apoRx;    // Sum the receive channels
    args.apoFocus = d_apoTx;    // Preserve the transmits
    args.nSynth = nchans;       // nchans samples to sum
    args.nFocus = nxmits;       // nxmits samples to keep
    args.strideSynth = nxmits;  // Each chan is separated by nxmits
    args.strideFocus = 1;       // Each xmit is separated by 1
  }

  // Call the focus_synth kernel. Explicitly specify the kernel template
  // parameters to achieve implicit template function instantiation. The
  // struct focusArgs and function focusHelper are used purely to make the
  // following code more readable.
  if (imode == IM::Linear) {
    if (isRF && isApo) focusHelper<IM::Linear, true, true>(&args);
    if (isRF && !isApo) focusHelper<IM::Linear, true, false>(&args);
    if (!isRF && isApo) focusHelper<IM::Linear, false, true>(&args);
    if (!isRF && !isApo) focusHelper<IM::Linear, false, false>(&args);
  } else if (imode == IM::Cubic) {
    if (isRF && isApo) focusHelper<IM::Cubic, true, true>(&args);
    if (isRF && !isApo) focusHelper<IM::Cubic, true, false>(&args);
    if (!isRF && isApo) focusHelper<IM::Cubic, false, true>(&args);
    if (!isRF && !isApo) focusHelper<IM::Cubic, false, false>(&args);
  } else if (imode == IM::Lanczos3) {
    if (isRF && isApo) focusHelper<IM::Lanczos3, true, true>(&args);
    if (isRF && !isApo) focusHelper<IM::Lanczos3, true, false>(&args);
    if (!isRF && isApo) focusHelper<IM::Lanczos3, false, true>(&args);
    if (!isRF && !isApo) focusHelper<IM::Lanczos3, false, false>(&args);
  }
}

template <typename T_in, typename T_out>
template <InterpMode imode, bool isRF, bool isApo>
void FocusSynAp<T_in, T_out>::focusHelper(focusArgs *args) {
  // Grab pointers and pitches
  DataDim odims = this->out.getDataDim();
  int delPitch = delTx.getNp();
  int outPitch = this->out.getNp();
  // Kernel launch parameters
  dim3 B(16, 4, 4);
  dim3 G((odims.x - 1) / B.x + 1, (odims.y - 1) / B.y + 1,
         (odims.c - 1) / B.z + 1);
  float normFactor =
      (typeid(T_in) == typeid(short2) && typeid(T_out) == typeid(float2))
          ? 32767.f
          : 1.f;

  // Loop through frames
  this->out.resetToZeros();
  for (int f = 0; f < raw->getNf(); f++) {
    CCE(cudaSetDevice(this->devID));
    CCE(cudaDestroyTextureObject(tex));
    initTextureObject(raw->getFramePtr(f));
    T_out *d_out = this->out.getFramePtr(f);
    kernels::focus_synth<T_out, imode, isRF, isApo><<<G, B, 0, this->stream>>>(
        tex, nrows, ncols, args->nFocus, args->strideFocus, args->delFocus,
        args->apoFocus, args->nSynth, args->strideSynth, args->delSynth,
        args->apoSynth, delPitch, d_out, outPitch, outwlps, normFactor,
        1.f / spc);
    getLastCudaError("Focusing kernel failed.\n");
  }
}

///////////////////////////////////////////////////////////////////////////
// kernels
///////////////////////////////////////////////////////////////////////////
// Core kernel with template options for conditional execution. Templatizing
// the kernel allows the compiler to make branch-wise optimizations at
// compile-time. That is, the GPU will not actually go through the if/else
// branches for template parameters, and will instead execute the correct
// compiled version for each branch.
template <typename T_out, IM imode, bool isRF, bool isApo>
__global__ void kernels::focus_synth(
    cudaTextureObject_t tex, int nrows, int ncols, int nFocus, int strideFocus,
    float *delFocus, float *apoFocus, int nSynth, int strideSynth,
    float *delSynth, float *apoSynth, int delPitch, T_out *out, int outPitch,
    float outwlps, float normFactor, float cyclesPerSample) {
  // Determine information about the current thread
  int row = blockIdx.x * blockDim.x + threadIdx.x;       // pixel row
  int col = blockIdx.y * blockDim.y + threadIdx.y;       // pixel column
  int idxFocus = blockIdx.z * blockDim.z + threadIdx.z;  // kept dimension
  // Boundary check
  if (row < nrows && col < ncols && idxFocus < nFocus) {
    // Move all pointers to the current row and column.
    delFocus += row + delPitch * col;
    delSynth += row + delPitch * col;
    if (isApo) apoFocus += row + delPitch * col;
    if (isApo) apoSynth += row + delPitch * col;
    out += row + outPitch * col;
    // No need to compute if the apodization is zero
    float apoF = 1.f, apoS = 1.f;
    if (isApo) apoF = apoFocus[delPitch * ncols * idxFocus];
    if (apoF == 0.f) {
      saveData2(out, outPitch * ncols * idxFocus, make_float2(0.f));
      return;
    }
    // Otherwise, compute everything
    float delF = delFocus[delPitch * ncols * idxFocus];
    float2 sum = make_float2(0.f);  // Initialize running sum
    for (int idxSynth = 0; idxSynth < nSynth; idxSynth++) {
      // No need to load data or compute if apodization is zero
      if (isApo) apoS = apoSynth[delPitch * ncols * idxSynth];
      if (apoS == 0.f) continue;
      // Otherwise, get the delay (x) and the transmit/channel (y) to retrieve
      float delS = delSynth[delPitch * ncols * idxSynth];
      float x = delF + delS;
      int y = strideFocus * idxFocus + strideSynth * idxSynth;
      // Interpolate
      float2 data;
      if (imode == IM::Linear) {
        data = interp1d::linear<float2>(tex, x, y);
      } else if (imode == IM::Cubic) {
        data = interp1d::cubic<float2>(tex, x, y);
      } else if (imode == IM::Lanczos3) {
        data = interp1d::lanczos3<float2>(tex, x, y);
      }
      if (isApo) data *= apoS;  // Apply apodization if provided
      if (!isRF) {     // If baseband, undo the initial phase due todemodulation
        float2 shift;  // Get the phase rotation complex phasor
        sincospif(2.0f * x * cyclesPerSample, &shift.y, &shift.x);
        data = complexMultiply(data, shift);  // Multiply
      }
      // Accumulate data to synthesize the aperture
      sum += data;
    }
    if (isApo) sum *= apoF;  // Apply apodization
    sum *= normFactor;  // Undo the normalization factor added by CUDA textures
    // float2 demod;       // Get the demodulation complex phasor
    // sincospif(-4.0f * row * outwlps, &demod.y, &demod.x);
    // sum = complexMultiply(sum, demod);  // Apply demodulation
    // Cast sum to correct datatype and store result in d_out
    saveData2(out, outPitch * ncols * idxFocus, sum);
  }
}
// Kernel to add scalar constant to entire delay table
__global__ void kernels::addGlobalDelay(DataDim dims, float *d_del,
                                        float globalDelay) {
  // Determine information about the current thread
  int row = blockIdx.x * blockDim.x + threadIdx.x;
  int col = blockIdx.y * blockDim.y + threadIdx.y;
  int page = blockIdx.z * blockDim.z + threadIdx.z;
  if (row < dims.x && col < dims.y && page < dims.c) {
    int idx = row + dims.p * (col + dims.y * page);
    d_del[idx] += globalDelay;
  }
}
template class FocusSynAp<short2, short2>;
template class FocusSynAp<short2, float2>;
template class FocusSynAp<float2, float2>;
}  // namespace rtbf
