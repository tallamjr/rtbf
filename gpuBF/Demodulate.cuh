/**
 @file gpuBF/Demodulate.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2021-05-06

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef DEMODULATE_CUH_
#define DEMODULATE_CUH_

#include "DataProcessor.cuh"
#include "vector_ops.cuh"

namespace rtbf {

/**	@brief Class to demodulate a data array.

This class demodulates complex data by multiplying by a complex exponential.
    Z_out[k] = Z_in[k] * exp(2j * pi * f * k),
where f is fc/fs (i.e., cycles per sample).

In-place demodulation can only be used if T_in and T_out are the same type.
*/
template <typename T_in, typename T_out>
class Demodulate : public DataProcessor<T_out> {
 private:
  DataArray<T_in> *in;  ///< Input DataArray
  bool in_place;        ///< Whether to operate in-place or out-of-place
  float cps;            ///< Cycles per sample (fc / fs)

 public:
  Demodulate();
  virtual ~Demodulate();

  /// @brief Initialize Demodulate object with a DataArray input
  void initialize(DataArray<T_in> *input, float samplesPerCycle,
                  bool inPlace = true, cudaStream_t cudaStream = 0,
                  int verbosity = 1);
  /// @brief Initialize Demodulate object with a DataProcessor input
  void initialize(DataProcessor<T_in> *input, float samplesPerCycle,
                  bool inPlace = true, cudaStream_t cudaStream = 0,
                  int verbosity = 1);
  /// @brief Reset object
  void reset();

  /// @brief Demodulate the signals
  void demodulate();
  /// @brief Set a new input DataArray
  void setInputDataArray(DataArray<T_in> *input);
  /// @brief Set a new input DataArray from a DataProcessor
  void setInputDataProcessor(DataProcessor<T_in> *input) {
    setInputDataArray(input->getOutputDataArray());
  }
  /// @brief Override getOutputDataArray function for in-place demodulation
  DataArray<T_out> *getOutputDataArray();
};

namespace DemodulateKernels {
template <typename T_in, typename T_out>
__global__ void demodData(T_in *idata, DataDim idims, T_out *odata,
                          DataDim odims, float cps);
}  // namespace DemodulateKernels
}  // namespace rtbf

#endif /* DEMODULATE_CUH_ */
